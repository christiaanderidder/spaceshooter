using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace SpaceShooter
{
    public class Asteroid : MoveableGameObject, ICollidable
    {
        private float _radius;
        private Model _model;
        
        // model rotations appart from physics
        private Quaternion _modelRotation;
        private float _modelJaw;
        private float _modelPitch;
        private float _modelRoll;

        private int _health;

        private float _moveJaw;
        private float _movePitch;
        private float _moveRoll;

        private bool _isColliding;
        private List<Bound> _bounds;
        private float _mass;
        private float _additionalVelocity;
        private Quaternion _additionalRotation;


        public Asteroid(Game game, Vector3 position, float scale): base(game)
        {
            this.Scale = scale;
            _modelJaw = 0;
            _modelPitch = 0;
            _modelRoll = 0;
            _moveJaw = 0;
            _movePitch = 0;
            _moveRoll = 0;

            _modelRotation = Quaternion.Identity;
            CreateBounds();

            _isColliding = false;
            _mass = 250f;
            _additionalVelocity = 0f;
            _additionalRotation = Quaternion.Identity;
            _health = 0;

            this.Velocity = 0;
            this.Mass = 250;
            this.Position = position;
        }

        public override void Update()
        {
            //using a regular rotation for the asteroid we give it a movement path so it will not stand still in space
            this.Yaw = _moveJaw;

            //the modelRotation allowes us rotate the asteroid around its axis without interfering with the asteroids regular movementpath
            _modelRotation *= Quaternion.CreateFromYawPitchRoll(_modelJaw, _modelPitch, _modelRoll);

            base.Update();

            foreach (Bound bound in _bounds)
            {
                bound.Update(this.Position, _modelRotation);
            }

            Vector3 additionalDirection = Vector3.Transform(Vector3.Forward, _additionalRotation);
            this.Position += additionalDirection * _additionalVelocity;

            if (_additionalVelocity > 0)
            {
                _additionalVelocity -= 0.2f;
            }
            if (_additionalVelocity < 0)
            {
                _additionalVelocity += 0.2f;
            }

            if (_additionalVelocity < 0.2f && _additionalVelocity > -0.2f)
            {
                _additionalVelocity = 0;
            }
        }

        public void Draw(Camera camera)
        {

            Matrix[] transforms = new Matrix[_model.Bones.Count];
            _model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in _model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {

                    effect.EnableDefaultLighting();
                    effect.World = Matrix.CreateScale(this.Scale) * Matrix.CreateFromQuaternion(_modelRotation) * Matrix.CreateTranslation(this.Position);
                    effect.View = camera.View;
                    effect.Projection = camera.Projection;

                }

                mesh.Draw();
            }
        }

        public void Collide(float targetVelocity, Quaternion targetRotation, float targetMass)
        {

            // almost the same collision as in Ship, only change is extra resets of model rotations and regular rotations
            this.Position -= this.Direction * this.Velocity; 
            Quaternion rotationReset = Quaternion.CreateFromYawPitchRoll(-Yaw, -Pitch, -Roll);
            Rotation *= rotationReset;
            Quaternion modelRotationReset = Quaternion.CreateFromYawPitchRoll(-_modelJaw, -_modelPitch, -_modelRoll);
            _modelRotation *= modelRotationReset;

            if (_isColliding == false)
            {
                float thisSpeed = this.Velocity;
                float otherSpeed = targetVelocity;


                if (thisSpeed < 0)
                {
                    thisSpeed *= -1;
                }

                if (targetVelocity < 0)
                {
                    otherSpeed *= -1;
                }


                float N = _mass + thisSpeed;
                float targetN = targetMass + otherSpeed;
                float Ntot = N + targetN;
                _additionalVelocity = (targetN / Ntot) * targetVelocity;

                this.Velocity = (_mass / (_mass * targetMass)) * this.Velocity;


                _additionalRotation = targetRotation;
                _isColliding = true;
            }
        }

        private void CreateBounds()
        {
            _bounds = new List<Bound>();
            
            Bound b1 = new Bound(800f * this.Scale);
            b1.OffSet = new Vector3(100f * this.Scale, 0, 0);
            _bounds.Add(b1);

            Bound b2 = new Bound(800f * this.Scale);
            b2.OffSet = new Vector3(-100f * this.Scale, -800f * this.Scale, 0);
            _bounds.Add(b2);

            Bound b3 = new Bound(750f * this.Scale);
            b3.OffSet = new Vector3(50f * this.Scale, 800f * this.Scale, 0);
            _bounds.Add(b3);
        }


        //getters and setters 
        public float Radius
        {
            get { return _radius; }
            set { _radius = value; }
        }
        public Quaternion ModelRotation
        {
            get { return _modelRotation; }
            set { _modelRotation = value; }
        }

        public Model Model
        {
            get { return _model; }
            set { _model = value; }
        }

        public float Mass
        {
            get { return _mass; }
            set { _mass = value; }
        }

        public Quaternion AdditionalRotation
        {
            get { return _additionalRotation; }
            set { _additionalRotation = value; }
        }

        public float AdditionalVelocity
        {
            get { return _additionalVelocity; }
            set { value = _additionalVelocity; }
        }

        public bool IsColliding
        {
            get { return _isColliding; }
            set { _isColliding = value; }
        }

        public int Health
        {
            get { return _health; }
            set { _health = value; }
        }

        public List<Bound> Bounds
        {
            get { return _bounds; }
            set { _bounds = value; }
        }

        public float ModelJaw
        {
            get { return _modelJaw; }
            set { _modelJaw = value; }
        }

        public float ModelPitch
        {
            get { return _modelPitch; }
            set { _modelPitch = value; }
        }

        public float ModelRoll
        {
            get { return _modelRoll; }
            set { _modelRoll = value; }
        }

        public float MoveJaw
        {
            get { return _moveJaw; }
            set { _moveJaw = value; }
        }

        public float MovePitch
        {
            get { return _movePitch; }
            set { _movePitch = value; }
        }

        public float MoveRoll
        {
            get { return _moveRoll; }
            set { _moveRoll = value; }
        }

    }
}
