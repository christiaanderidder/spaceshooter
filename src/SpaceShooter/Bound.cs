﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SpaceShooter
{

    // Bounds are BoundingSpheres with a set offset making them easily updatable
    public class Bound
    {
        private Vector3 _offSet;
        private BoundingSphere _boundingSphere;
        private float _radius;

        public Bound(float radius)
        {
            _boundingSphere = new BoundingSphere();
            _offSet = Vector3.Zero;
            _boundingSphere.Radius = radius;
            _radius = radius;
        }

        public void Update(Vector3 position, Quaternion rotation)
        {
            _boundingSphere.Radius = _radius;
            _boundingSphere.Center = position + Vector3.Transform(_offSet, rotation);
        }

        public Vector3 OffSet
        {
            get { return _offSet; }
            set { _offSet = value; }
        }

        public float Radius
        {
            get { return _radius; }
            set { _radius = value; }
        }

        public BoundingSphere BoundingSphere
        {
            get { return _boundingSphere; }
            set { _boundingSphere = value; }
        }
    }
}
