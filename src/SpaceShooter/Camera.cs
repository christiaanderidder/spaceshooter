﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SpaceShooter
{
    public class Camera : MoveableGameObject
    {
        private Matrix _view;
        private Matrix _projection;
        private Vector3 _lookAt;
        
        private float _aspectRatio;
        private float _fieldOfView;
        private float _nearPlaneDistance;
        private float _farPlaneDistance;


        //This is a static camera wich can be plased at any location in the battlefield. The camera's view and projection are needed for all drawable 3d objects
        public Camera(Game game) : base (game)
        {
            _view = Matrix.Identity;
            _projection = Matrix.Identity;

            //the point in space the camera is looking at. Can be a point in space or a flying object such as an asteroid or a ship
            _lookAt = Vector3.Zero;
            this.Up = Vector3.Up;
            _aspectRatio = 4 / 3;
            _fieldOfView = MathHelper.ToRadians(65);

            //sets a minimun and maximum viewing distance
            _nearPlaneDistance = 1f;
            _farPlaneDistance = 500000f;
        }

        public override void Update()
        {
            _view = Matrix.CreateLookAt(this.Position, _lookAt, this.Up);
            _projection = Matrix.CreatePerspectiveFieldOfView(_fieldOfView, _aspectRatio, _nearPlaneDistance, _farPlaneDistance);
            base.Update();
        }

        public Matrix View
        {
            get { return _view; }
        }
        public Matrix Projection
        {
            get { return _projection; }
        }
        public Vector3 LookAt
        {
            get { return _lookAt; }
            set { _lookAt = value; }
        }

        public float FieldOfView
        {
            get { return _fieldOfView; }
            set { _fieldOfView = MathHelper.ToRadians(value); }
        }
        public float AspectRatio
        {
            get { return _aspectRatio; }
            set { _aspectRatio = value; }
        }
        public float NearPlaneDistance
        {
            get { return _nearPlaneDistance; }
            set { _nearPlaneDistance = value; }
        }
        public float FarPlaneDistance
        {
            get { return _farPlaneDistance; }
            set { _farPlaneDistance = value; }
        }
    }
}
