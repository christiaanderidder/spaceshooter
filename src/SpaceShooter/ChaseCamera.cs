﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SpaceShooter
{
    public class ChaseCamera : Camera
    {
        //chasecam vars
        private Vector3 _lookAtOffset;
        private Vector3 _positionOffset;
        private Vector3 _desiredPosition;

        private MoveableGameObject _chaseTarget;

        //chasecam physics
        private float stiffness;
        private float damping;
        private float mass;
        private Vector3 velocity;

        // a chaseCamera has all the functions needed to chase a target. the physics give it a nice stretching effect
        public ChaseCamera(Game game) : base (game)
        {
            _lookAtOffset = new Vector3(0.0f, 150.0f, 0.0f);
            _positionOffset = new Vector3(0.0f, 2000.0f, 3500.0f);
            _desiredPosition = Vector3.Zero;

            stiffness = 1800.0f;
            damping = 600.0f;
            mass = 50.0f;
            velocity = Vector3.Zero;
        }


        public override void Update()
        {
            // updates world positions
            UpdateWorldPositions();

            float elapsed = 1/60f;
            Vector3 strech = this.Position - _desiredPosition;
            Vector3 force = -stiffness * strech - damping * velocity;

            Vector3 acceleration = force / mass;
            velocity += acceleration * elapsed;

            this.Position += velocity * elapsed;

            base.Update();
            
        }

        private void UpdateWorldPositions()
        {
            Matrix transforms = Matrix.Identity;
            transforms.Forward = _chaseTarget.Direction;
            transforms.Up = _chaseTarget.Up;
            // this is important to make sure the camera doesnt show the ship upside down, though this could be usefull in an environment with gravity 
            this.Up = _chaseTarget.Up;
            transforms.Right = Vector3.Cross(Vector3.Up, _chaseTarget.Direction);

            //the position we want the chasecam to have with a desired offset from the chasetarget
            _desiredPosition = _chaseTarget.Position + Vector3.TransformNormal(_positionOffset, transforms);

            this.LookAt = _chaseTarget.Position + Vector3.TransformNormal(_lookAtOffset, transforms);
        }

        public MoveableGameObject ChaseTarget{
            get{return _chaseTarget;}
            set{_chaseTarget = value;}
        }

        public Vector3 OffSet
        {
            get { return _positionOffset; }
            set { _positionOffset = value; }
        }

    }
}
