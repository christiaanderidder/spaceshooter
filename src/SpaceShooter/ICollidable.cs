﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SpaceShooter
{
    public interface ICollidable
    {
        void Collide(float targetVelocity, Quaternion targetRotation, float targetMass);

        float Mass
        {
            get;
            set;
        }

        int Health
        {
            get;
            set;
        }

        Quaternion AdditionalRotation
        {
            get;
            set;
        }

        List<Bound> Bounds
        {
            get;
            set;
        }

        float AdditionalVelocity
        {
            get;
            set;
        }

        float Velocity
        {
            get;
            set;
        }

        Quaternion Rotation
        {
            get;
            set;
        }

        bool IsColliding
        {
            get;
            set;
        }

        Vector3 Position
        {
            get;
            set;
        }


    }
}
