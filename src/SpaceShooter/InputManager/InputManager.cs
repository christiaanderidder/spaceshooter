using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace SpaceShooter
{
    public enum InputState{
        Pressed,
        Down,
        Up
    }

    public class InputManager
    {
        private GamePadState[] _previousGPState = new GamePadState[4];
        private GamePadState[] _currentGPState = new GamePadState[4];

        private KeyboardState _previousKBState = new KeyboardState();
        private KeyboardState _currentKBState = new KeyboardState();

        public void Update()
        {
            //Update GamePadState for all gamepads and set previous state
            for(int i = 0; i < 4; i++)
            {
                _previousGPState[i] = _currentGPState[i];
                _currentGPState[i] = Microsoft.Xna.Framework.Input.GamePad.GetState((PlayerIndex)i); 
            }

            //Update KeyBoardState and set previous state
            _previousKBState = _currentKBState;
            _currentKBState = Microsoft.Xna.Framework.Input.Keyboard.GetState();
        }

        #region GamePad

        //Check if a button is pressed for any gamepad
        public Boolean GamePad(Buttons b)
        {
            return GamePad(b, InputState.Pressed);
        }

        //Check if a button has a given state for any gamepad
        public Boolean GamePad(Buttons b, InputState state)
        {
            switch (state)
            {
                case InputState.Pressed:
                    return (GamePad(b, PlayerIndex.One, state) || GamePad(b, PlayerIndex.Two, state) || GamePad(b, PlayerIndex.Three, state) || GamePad(b, PlayerIndex.Four, state));
                case InputState.Down:
                    return (GamePad(b, PlayerIndex.One, state) || GamePad(b, PlayerIndex.Two, state) || GamePad(b, PlayerIndex.Three, state) || GamePad(b, PlayerIndex.Four, state));
                case InputState.Up:
                    return (GamePad(b, PlayerIndex.One, state) || GamePad(b, PlayerIndex.Two, state) || GamePad(b, PlayerIndex.Three, state) || GamePad(b, PlayerIndex.Four, state));
                default:
                    return false;
            }
        }
        
        //Check if a button is pressed by a given gamepad
        public Boolean GamePad(Buttons b, PlayerIndex player)
        {
            return GamePad(b, player, InputState.Pressed);
        }

        //Check if a button has a given state for a given gamepad
        public Boolean GamePad(Buttons b, PlayerIndex player, InputState state)
        {
            switch(state)
            {
                case InputState.Pressed:
                    return (_currentGPState[(int)player].IsButtonDown(b) && _previousGPState[(int)player].IsButtonUp(b));
                case InputState.Down:
                    return (_currentGPState[(int)player].IsButtonDown(b));
                case InputState.Up:
                    return (_previousGPState[(int)player].IsButtonUp(b));
                default:
                    return false;
            }
        }

        #endregion

        #region Keyboard

        //Check if a key is pressed
        public Boolean Keyboard(Keys k)
        {
            return Keyboard(k, InputState.Pressed);
        }

        //Check if a key has a given state
        public Boolean Keyboard(Keys k, InputState state)
        {
            switch (state)
            {
                case InputState.Pressed:
                    return (_currentKBState.IsKeyDown(k) && _previousKBState.IsKeyUp(k));
                case InputState.Down:
                    return (_currentKBState.IsKeyDown(k));
                case InputState.Up:
                    return (_previousKBState.IsKeyUp(k));
                default:
                    return false;
            }
            
        }

        #endregion

        #region Presets

        #region Menu

        public Boolean MenuUp()
        {
            return (GamePad(Buttons.LeftThumbstickUp) || GamePad(Buttons.DPadUp) || Keyboard(Keys.Up));
        }

        public Boolean MenuUpHold()
        {
            return (GamePad(Buttons.LeftThumbstickUp, InputState.Down) || GamePad(Buttons.DPadUp, InputState.Down) || Keyboard(Keys.Up, InputState.Down));
        }

        public Boolean MenuDown()
        {
            return (GamePad(Buttons.LeftThumbstickDown) || GamePad(Buttons.DPadDown) || Keyboard(Keys.Down));
        }

        public Boolean MenuDownHold()
        {
            return (GamePad(Buttons.LeftThumbstickDown, InputState.Down) || GamePad(Buttons.DPadDown, InputState.Down) || Keyboard(Keys.Down, InputState.Down));
        }

        public Boolean MenuSubmit()
        {
            return (GamePad(Buttons.A) || GamePad(Buttons.Start) || Keyboard(Keys.Enter));
        }

        public Boolean MenuBack()
        {
            return (GamePad(Buttons.B) || GamePad(Buttons.Back) || Keyboard(Keys.Escape));
        }

        #endregion

        #region Ship

        public float ShipJaw(PlayerIndex i)
        {
            if (GamePad(Buttons.LeftShoulder, i, InputState.Down) || Keyboard(Keys.A, InputState.Down))
            {
                return 1f;
            }
            else if (GamePad(Buttons.RightShoulder, i, InputState.Down) || Keyboard(Keys.D, InputState.Down))
            {
                return -1f;
            }
            else
            {
                return 0f;
            }

        }

        public float ShipPitch(PlayerIndex i)
        {
            if (Keyboard(Keys.Down, InputState.Down))
            {
               return -1f;
            }
            else if (Keyboard(Keys.Up, InputState.Down))
            {
               return 1f;
            }
            else
            {
               return GetGamePadState(i).ThumbSticks.Left.Y;
            }
        }

        public float ShipRoll(PlayerIndex i)
        {
            if (Keyboard(Keys.Right, InputState.Down))
            {
                return 1f;
            }
            else if (Keyboard(Keys.Left, InputState.Down))
            {
                return -1f;
            }
            else
            {
                return GetGamePadState(i).ThumbSticks.Left.X;
            }
        }

        public float ShipAccelerate(PlayerIndex i)
        {
            if (Keyboard(Keys.W, InputState.Down))
            {
                return 1f;
            }
            else
            {
                return GetGamePadState(i).Triggers.Right;
            }
        }

        public float ShipDecelerate(PlayerIndex i)
        {
            if (Keyboard(Keys.S, InputState.Down))
            {
                return -1f;
            }
            else
            {
                return GetGamePadState(i).Triggers.Left;
            }
        }

        #endregion

        public Boolean GamePause()
        {
            return (GamePad(Buttons.Start) || Keyboard(Keys.Escape));
        }


        #endregion
        
        //Getters for state (triggers, sticks etc)
        public GamePadState GetGamePadState(PlayerIndex player)
        {
            return _currentGPState[(int)player];
        }
    }
}
