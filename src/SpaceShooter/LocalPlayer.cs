﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace SpaceShooter
{
    public class LocalPlayer : Player
    {
        private ChaseCamera _chaseCamera;
        private Viewport _viewport;
        private PlayerIndex _playerIndex;
        private Navigation _navigation;
        private SkyBox _skyBox;


        public LocalPlayer(Game game) : base(game)
        {
            _chaseCamera = new ChaseCamera(game);
            _chaseCamera.ChaseTarget = this.Ship;
            _viewport = new Viewport();
            _skyBox = new SkyBox(game);
            Ship.IsLocal = true;
        }

        public override void Update()
        {
            _chaseCamera.Update();
            base.Update();
        }

        public ChaseCamera ChaseCamera
        {
            get { return _chaseCamera; }
            set { _chaseCamera = value; }
        }

        public SkyBox SkyBox
        {
            get { return _skyBox; }
            set { _skyBox = value; }
        }

        public Viewport Viewport
        {
            get { return _viewport; }
            set { _viewport = value;
            _chaseCamera.AspectRatio = _viewport.AspectRatio;
            }
        }

        public PlayerIndex PlayerIndex
        {
            get { return _playerIndex; }
            set { _playerIndex = value;
            this.Ship.PlayerIndex = _playerIndex;
            }
        }

        public Navigation Navigation
        {
            get { return _navigation; }
            set { _navigation = value; }
        }
    }
}
