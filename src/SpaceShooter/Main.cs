using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SpaceShooter
{
    public class Main : Microsoft.Xna.Framework.Game
    {
        private GraphicsDeviceManager graphics;
        private ScreenManager _screenManager;

        public Main()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;
            graphics.PreferMultiSampling = true;
            Content.RootDirectory = "Content";
            Components.Add(new GamerServicesComponent(this));
            _screenManager = new ScreenManager(this);
            _screenManager.AddScreen(new BackgroundScreen());
            _screenManager.AddScreen(new MainMenuScreen());
            Components.Add(_screenManager);
        }

        protected override void Initialize()
        {
            
            base.Initialize();
        }

        protected override void Update(GameTime gameTime)
        {
            /*switch(_gs){
                case GameState.AwesomeLogo:
                    if (!_isInitialized)
                    {
                        _screenManager.AddScreen(new AwesomeLogoScreen());
                        _isInitialized = true;
                    }
                    break;

                case GameState.MainMenu:
                    _screenManager.AddScreen(new MainMenuScreen());
                    break;
                case GameState.SignInMenu:
                    _screenManager.AddScreen(new SignInMenuScreen());
                    break;
                case GameState.Playing:
                    if (!_isInitialized)
                    {
                        _screenManager.AddScreen(new GameScreen());
                        _isInitialized = true;
                    }
                    break;
            }*/
            
           base.Update(gameTime);
        }

    }
}
