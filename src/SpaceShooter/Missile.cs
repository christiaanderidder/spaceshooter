﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpaceShooter
{
    public class Missile : MoveableGameObject
    {
        private List<Bound> _bounds;
        private Vector3 _missileOffset;
        private float _radius;
        private bool _hasHit;
        private bool _dead;
        private int _deadCount;
        private Model _model;
        private int _power;

        public Missile(Game game, Model model): base(game)
        {
            _model = model;
            _radius = 150;
            _deadCount = 0;
            _dead = false;
            _hasHit = false;
            _bounds = new List<Bound>();
            _power = 0;

            Bound bound = new Bound(150f);
            _bounds.Add(bound);
        }

        //resets the missiles variables, used after a missile is used when added to the inactive list
        public void Reset()
        {
            _radius = 150;
            this.Position = Vector3.Zero;
            this.Direction = Vector3.Zero;
            this.Rotation = Quaternion.Identity;
            this.World = Matrix.Identity;
            this.Velocity = 0;
            this.Scale = 2f;
            
            _dead = false;
            _missileOffset = Vector3.Zero;
            _bounds.Clear();
            Bound bound = new Bound(150f);
            bound.Radius = _radius;
            _bounds.Add(bound);

            _deadCount = 0;
            _hasHit = false;
        }

        public override void Update()
        {
            //checks if the missile has hit something, if yes, expend the boundingsphere!!! blow it all up!
            if (_hasHit == true)
            {
                _radius *= 1.10f;

                _deadCount++;
                if(_deadCount > 15){
                    _dead = true;
                }
            }
            
            base.Update();
            foreach (Bound bound in _bounds)
            {
                bound.Radius = _radius;
                bound.Update(this.Position, this.Rotation);
            }

            
        }

        public void Draw(Camera camera)
        {
           //until a missileModel is added we'll use the missiles boundingspheres... but this does give a nice effect though
            foreach (Bound bound in _bounds)
            {
                BoundingSphereRenderer.Render(bound.BoundingSphere, this.Game.GraphicsDevice, camera.View, camera.Projection, Color.Red);
            }

            //the code is ready to receive a missile model

            /*Matrix[] transforms = new Matrix[_model.Bones.Count];
            _model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in _model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.World = Matrix.CreateScale(this.Scale) * Matrix.CreateFromQuaternion(this.Rotation) * Matrix.CreateTranslation(this.Position);
                    effect.View = camera.View;
                    effect.Projection = camera.Projection;
                }

                //mesh.Draw();
            }*/
        }

        //handles missile Collisiong, dameging objects with the iCollidable interface also setting the _hasHit to true, so the boundingsphere can 'expland'
        public void Collide(ICollidable mgo)
        {
            if (_hasHit == false)
            {
                _power = (int)(_radius / 25);
                mgo.Health -= _power;
            }
            this.Velocity = 0;
            _hasHit = true;
        }


        //getters and setters
        public Vector3 missileOffset
        {
            get { return _missileOffset; }
            set { _missileOffset = value; }
        }
        public float Radius
        {
            get { return _radius; }
            set { _radius = value; }
        }
        public bool HasHit
        {
            get { return _hasHit; }
            set { _hasHit = value; }
        }
        public bool Dead
        {
            get { return _dead; }
            set { _dead = value; }
        }

        public Model Model
        {
            get { return _model; }
            set { _model = value; }
        }

        public List<Bound> Bounds
        {
            get { return _bounds; }
            set { _bounds = value; }
        }
    }
}
