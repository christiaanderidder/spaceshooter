using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace SpaceShooter
{
    public class MoveableGameObject
    {
        private Vector3 _position;
        private Vector3 _direction;
        private Vector3 _up;

        private float _yaw;
        private float _pitch;
        private float _roll;

        private Quaternion _rotation;
        private float _velocity;
        private float _scale;

        private List<BoundingSphere> _boundingSpheres;
        private Game _game;

        private Matrix _world;

        // MovableGameObject is a nice base for all 3D objects in the game having most variables and functionality needed to position an object in 3D space
        public MoveableGameObject(Game game)
        {
            _game = game;

            _position = Vector3.Zero;
            _direction = Vector3.Zero;

            _yaw = 0f;
            _pitch = 0f;
            _roll = 0f;

            _rotation = Quaternion.Identity;
            _boundingSpheres = new List<BoundingSphere>();

            _velocity = 0f;
            _world = Matrix.Identity;
            _scale = 2f;
        }

        public virtual void Update()
        {
            Quaternion addedRotation = Quaternion.CreateFromYawPitchRoll(_yaw, _pitch, _roll);
            _rotation *= addedRotation;

            _direction = Vector3.Transform(Vector3.Forward, _rotation);
            _up = Vector3.Transform(Vector3.Up, _rotation);
            _position += _direction * _velocity;
            _world = Matrix.CreateScale(_scale) * Matrix.CreateFromQuaternion(_rotation) * Matrix.CreateTranslation(_position);
        }

        public Vector3 Position
        {
            get { return _position; }
            set { _position = value; }
        }

        public Vector3 Direction
        {
            get { return _direction; }
            set { _direction = value; }
        }
        public Vector3 Up
        {
            get { return _up; }
            set { _up = value; }
        }
        public float Scale
        {
            get { return _scale; }
            set { _scale = value; }
        }
        public float Velocity
        {
            get { return _velocity; }
            set { _velocity = value; }
        }
        public float Yaw
        {
            get { return _yaw; }
            set { _yaw = value; }
        }
        public float Pitch
        {
            get { return _pitch; }
            set { _pitch = value; }
        }
        public float Roll
        {
            get { return _roll; }
            set { _roll = value; }
        }

        public Matrix World
        {
            get { return _world; }
            set { _world = value; }
        }
        public Quaternion Rotation
        {
            get { return _rotation; }
            set { _rotation = value; }
        }

        public Game Game
        {
            get { return _game; }
            set { _game = value; }
        }
    }
}
