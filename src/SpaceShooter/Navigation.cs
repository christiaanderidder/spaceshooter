﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpaceShooter
{
    public class Navigation
    {
        private Texture2D _backgroundTexture;
        private Texture2D _shipTexture;
        
        private Vector2 _backGroundPosition;
        private Vector2 _startPosition;

        private Vector2 _currentPosition;
        private List<Vector2> _enemyPositions;

        private float _shipScale;
        private float _backGroundScale;
        private float _backGroundSize;

        private SpriteBatch _spriteBatch;
        private Viewport _viewPort;


        public Navigation(SpriteBatch sb, Viewport vp)
        {
            _spriteBatch = sb;
            _viewPort = vp;
            _backGroundPosition = Vector2.Zero;
            _startPosition = Vector2.Zero;
            _currentPosition = Vector2.Zero;
            _enemyPositions = new List<Vector2>();
        }


        public void SetTextures()
        {
            float vpScale = (float)(_viewPort.Width / 640f);
            _shipScale = (10f / (float)_shipTexture.Width) * vpScale;
            _backGroundScale = (300f / (float)_backgroundTexture.Width) * vpScale;
            _backGroundSize = (_backgroundTexture.Width / 2) * vpScale;

            _backGroundPosition.X = _viewPort.Width - (int)(_backgroundTexture.Width * _backGroundScale);
            _backGroundPosition.Y =  _viewPort.Height - (int)(_backgroundTexture.Height * _backGroundScale);
            _startPosition.X = _backGroundPosition.X + ((float)_backgroundTexture.Width*_backGroundScale / 2) - ((float)_shipTexture.Width*_shipScale / 2) ;
            _startPosition.Y = _backGroundPosition.Y + ((float)_backgroundTexture.Height * _backGroundScale / 2) - ((float)_shipTexture.Height * _shipScale / 2);


            Console.WriteLine(_shipScale);

            
        }

        public void Draw()
        {
            _spriteBatch.Draw(_backgroundTexture, _backGroundPosition, null, Color.Red, 0, Vector2.Zero, _backGroundScale, SpriteEffects.None, 1);
            _spriteBatch.Draw(_shipTexture, _startPosition, null, Color.Blue, 0,  (_currentPosition)/_shipScale, _shipScale, SpriteEffects.None, 1);

            foreach (Vector2 enemyPosition in _enemyPositions)
            {
                _spriteBatch.Draw(_shipTexture, _startPosition, null, Color.Red, 0, (enemyPosition) / _shipScale, _shipScale, SpriteEffects.None, 1);
            }
        }

        public void Update(List<LocalPlayer> players, Ship ship)
        {
            float range = ((_backGroundSize / 5f) * 3) / 2;
            _enemyPositions.Clear();
            foreach (LocalPlayer player in players)
            {
                if (player.Ship != ship)
                {
                    Vector2 position = new Vector2();
                    position.X = (player.Ship.Position.X / 100000) * range;
                    position.Y = (player.Ship.Position.Z / 100000) * range;
                    _enemyPositions.Add(position);
                }
            }
            

            _currentPosition.X = (ship.Position.X / 100000) * range;
            _currentPosition.Y = (ship.Position.Z / 100000) * range;
        }

        public Texture2D BackgroundTexture
        {
            get { return _backgroundTexture; }
            set { _backgroundTexture = value; }
        }

        public Texture2D ShipTexture
        {
            get { return _shipTexture; }
            set { _shipTexture = value; }
        }

    }
}
