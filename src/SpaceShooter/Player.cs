using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace SpaceShooter
{
    public class Player
    {
        private Ship _ship;
        private int _kills;
        private int _deaths;
        private Game _game;
        private string _name;


        public Player(Game game)
        {
            _ship = new Ship(game);
            _name = "enemy";
            _game = game;
            _kills = 0;
            _deaths = 0;
        }

        public virtual void Update()
        {
            _ship.Update();
        }

        public void HandleInput()
        {
            _ship.HandleInput();
        }

        public Ship Ship
        {
            get { return _ship; }
            set { _ship = value; }
        }

        public int Kills
        {
            get { return _kills; }
            set { _kills = value; }
        }

        public int Deaths
        {
            get { return _deaths; }
            set { _deaths = value; }
        }

        public Game Game
        {
            get { return _game; }
            set { _game = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
    }
}