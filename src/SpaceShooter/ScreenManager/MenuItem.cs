﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpaceShooter
{
    class MenuItem
    {
        public MenuItem(String label){
            _label = label.ToUpper();
        }

        public event EventHandler Pressed;

        public void IsPressed()
        {
            if (Pressed != null)
            {
                Pressed(this, new EventArgs());
            }
        }

        private String _label = "";
        private Boolean _isSelected = false;

        public String Label
        {
            get { return _label; }
            set { _label = value.ToUpper(); }
        }

        public Boolean IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }
    }
}
