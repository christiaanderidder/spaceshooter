﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace SpaceShooter
{
    class MenuScreen : Screen
    {
        private List<MenuItem> _menuItems = new List<MenuItem>();

        private Texture2D _itemTexture = null;
        private Color _itemTextureColor = Color.White;
        private Color _itemTextColor = Color.White;

        private Texture2D _itemSelectedTexture = null;
        private Color _itemSelectedTextureColor = Color.White;
        private Color _itemSelectedTextColor = Color.White;
        
        private SoundEffect _itemSound;

        private float _itemSpacing = 20;
        
        private int _selectedIndex = 0;
        private double _timer = 0;

        public override void LoadContent()
        {
            Content = new ContentManager(ScreenManager.Game.Services, "Content");
            Texture2D blank = Content.Load<Texture2D>("Textures/blank");
            _itemTexture = blank;
            _itemSelectedTexture = blank;
        }

        public override void HandleInput(GameTime gameTime)
        {
            double additionaltime = gameTime.ElapsedGameTime.TotalMilliseconds;

            _timer += additionaltime;

            //If Up or Down are held down or pressed, and 150 milliseconds have passed since the last input (because otherwise it will change every 1/60 of a second) 
            if ((Input.MenuUpHold() && _timer > 150) || Input.MenuUp())
            {
                _timer = 0;
                _selectedIndex--;
                _itemSound.Play();
                if (_selectedIndex < 0)
                {
                    _selectedIndex = _menuItems.Count() - 1;
                }
            }
            else if ((Input.MenuDownHold() && _timer > 150) || Input.MenuDown())
            {
                _timer = 0;
                _selectedIndex++;
                _itemSound.Play();
                if (_selectedIndex > _menuItems.Count() - 1)
                {
                    _selectedIndex = 0;
                }
            }

            //Set active menu item
            foreach (MenuItem i in _menuItems)
            {
                i.IsSelected = false;
            }
            _menuItems[_selectedIndex].IsSelected = true;

            //If the submit button is pressed, tell the active menu item to fire the Pressed event
            if (Input.MenuSubmit())
            {
                _menuItems[_selectedIndex].IsPressed();
            }

        }

        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime)
        {
            SpriteBatch spriteBatch  = ScreenManager.SpriteBatch;
            GraphicsDevice gd = ScreenManager.GraphicsDevice;
            Viewport viewport = gd.Viewport;

            spriteBatch.Begin();

            int numOfMenuItems = _menuItems.Count;
            Vector2 menuItemPosition = new Vector2(40, 400);

            for (int i = 0; i < numOfMenuItems; i++ )
            {
                
                MenuItem mi = _menuItems[i];
                Vector2 menuItemLabelDimension = ScreenManager.Font.MeasureString(mi.Label);
                Vector2 menuItemLabelPosition;

                if (mi.IsSelected)
                {
                    menuItemLabelPosition = new Vector2((_itemSelectedTexture.Width / 2) - (menuItemLabelDimension.X / 2), menuItemPosition.Y + (_itemSelectedTexture.Height / 2) - (menuItemLabelDimension.Y / 2));
                    menuItemLabelPosition.X = _itemSelectedTexture.Bounds.X + 80;
                    spriteBatch.Draw(_itemSelectedTexture, menuItemPosition, _itemSelectedTextureColor);
                    spriteBatch.DrawString(ScreenManager.Font, mi.Label, menuItemLabelPosition, _itemSelectedTextColor);
                    menuItemPosition.Y += _itemSelectedTexture.Height + _itemSpacing;
                }
                else
                {
                    menuItemLabelPosition = new Vector2((_itemTexture.Width / 2) - (menuItemLabelDimension.X / 2), menuItemPosition.Y + (_itemTexture.Height / 2) - (menuItemLabelDimension.Y / 2));
                    menuItemLabelPosition.X = _itemTexture.Bounds.X + 80;
                    spriteBatch.Draw(_itemTexture, menuItemPosition, _itemTextureColor);
                    spriteBatch.DrawString(ScreenManager.Font, mi.Label, menuItemLabelPosition, _itemTextColor);
                    menuItemPosition.Y += _itemTexture.Height + _itemSpacing;
                }

            }

            spriteBatch.End();

            gd.BlendState = BlendState.Opaque;
            gd.DepthStencilState = DepthStencilState.Default;
            gd.SamplerStates[0] = SamplerState.LinearWrap;
        }

        public List<MenuItem> MenuItems
        {
            get { return _menuItems;  }
            set { _menuItems = value; }
        }

        public SoundEffect ItemSound
        {
            get { return _itemSound; }
            set { _itemSound = value; }
        }

        public Texture2D ItemTexture
        {
            get { return _itemTexture; }
            set { _itemTexture = value; }
        }
        public Color ItemTextureColor
        {
            get { return _itemTextureColor; }
            set { _itemTextureColor = value; }
        }
        public Color ItemTextColor
        {
            get { return _itemTextColor; }
            set { _itemTextColor = value; }
        }

        public Texture2D ItemSelectedTexture
        {
            get { return _itemSelectedTexture; }
            set { _itemSelectedTexture = value; }
        }
        public Color ItemSelectedTextureColor
        {
            get { return _itemSelectedTextureColor; }
            set { _itemSelectedTextureColor = value; }
        }
        public Color ItemSelectedTextColor
        {
            get { return _itemSelectedTextColor; }
            set { _itemSelectedTextColor = value; }
        }

        public float ItemSpacing
        {
            get { return _itemSpacing; }
            set { _itemSpacing = value; }
        }
    }
}
