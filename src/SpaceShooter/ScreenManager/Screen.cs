﻿using System;
using Microsoft.Xna.Framework;
using System.IO;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.GamerServices;
using System.Collections.Generic;

namespace SpaceShooter
{
    public enum ScreenState
    {
        Active, //Screen is active: Screen does handle input and does draw and update itself.
        Inactive, //Screen is not active, does not handle input, draw or update itself, but keeps a state saved and is ready to be activated
        Covered, //Screen is covered: Screen doesn't handle input but does update and draw itself.
    }

    public class Screen
    {
        private ScreenState _screenState = ScreenState.Inactive;
        private ScreenManager _screenManager;
        private ContentManager _content;
        private InputManager _input;

        private List<Viewport> _viewPorts = new List<Viewport>(); 

        public virtual void LoadContent() { }
        public virtual void UnloadContent() { }

        public virtual void Initialize() { } 
        public virtual void Update(GameTime gameTime) { }
        public virtual void HandleInput(GameTime gameTime) { }
        public virtual void Draw(GameTime gameTime) { }

        public ScreenState ScreenState
        {
            get { return _screenState; }
            set { _screenState = value; }
        }

        public ScreenManager ScreenManager
        {
            get { return _screenManager; }
            internal set { _screenManager = value; }
        }

        public ContentManager Content
        {
            get { return _content; }
            set { _content = value; }
        }

        public InputManager Input
        {
            get { return _input; }
            set { _input = value; }
        }
    }
}
