using System;
using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections;

namespace SpaceShooter
{
    public class ScreenManager : Microsoft.Xna.Framework.DrawableGameComponent
    {
        private List<Screen> _screens = new List<Screen>();
        private List<Screen> _screensToUpdate = new List<Screen>();
        private GraphicsDevice _graphicsDevice;

        private SpriteBatchExtended _spriteBatch;
        private SpriteFont _font;
        private SpriteFont _normalFont;
        private SpriteFont _glyphFont;

        private ContentManager _content;

        private InputManager _input;

        private bool _isInitialised = false;

        public ScreenManager(Game game) : base(game)
        {

            
        }

        public override void Initialize()
        {
            base.Initialize();
            _input = new InputManager();

            //Initialize added screens
            foreach (Screen s in _screens)
            {
                s.Initialize();
                s.Input = _input;
            }
            _isInitialised = true;
        }

        protected override void LoadContent()
        {
            _graphicsDevice = GraphicsDevice;
            _content = Game.Content;
            _spriteBatch = new SpriteBatchExtended(GraphicsDevice);

            _font = _content.Load<SpriteFont>("Fonts/coalition");
            _normalFont = _content.Load<SpriteFont>("Fonts/menufont");
            _glyphFont = _content.Load<SpriteFont>("Fonts/buttons");
            _spriteBatch.fntButton = _glyphFont;

            //Load content for all initialized screens
            foreach (Screen s in _screens)
            {
                s.LoadContent();
            }

            base.LoadContent();
        }

        protected override void UnloadContent()
        {
            // Tell each of the screens to unload their content.
            foreach (Screen s in _screens)
            {
                s.UnloadContent();
            }
        }

        public override void Update(GameTime gameTime)
        {
            _input.Update();
 
            _screensToUpdate.Clear();
            _screensToUpdate = new List<Screen>(_screens);

            //If the are screens, set the top most screen as Active
            if (_screensToUpdate.Count != 0)
            {
                _screensToUpdate[_screensToUpdate.Count - 1].ScreenState = ScreenState.Active;
            }

            foreach (Screen s in _screensToUpdate)
            {
                //Only the active Screen will handle input and get updated.
                if (s.ScreenState == ScreenState.Active)
                {
                    if (Game.IsActive)
                    {
                        s.HandleInput(gameTime);
                    }
                    s.Update(gameTime);
                }
                //Covered screens will be updated (e.g. when a pausemenu is visible, the game will still update because it's a network game)
                else if (s.ScreenState == ScreenState.Covered)
                {
                    s.Update(gameTime);
                }
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            foreach (Screen s in _screens)
            {
                //Active and Covered screens will be drawn, Inactive screens will not.
                if (s.ScreenState == ScreenState.Active || s.ScreenState == ScreenState.Covered)
                {
                    s.Draw(gameTime);
                }
            }
        }

        //Removes current screen and sets the screen below it to Active
        public void PreviousScreen()
        {
            if (_screensToUpdate.Count != 0)
            {
                //If the screen below the current screen is the game Background, the current screen can't be removed.
                if (!(_screensToUpdate[_screensToUpdate.Count - 2] is BackgroundScreen))
                {
                    RemoveScreen(_screensToUpdate[_screensToUpdate.Count - 1]);
                }
            }
        }

        public void AddScreen(Screen s)
        {
            s.ScreenManager = this;
            s.Input = _input;
            //check if ScreenManager is already initialized. If so, the added screen won't be initialized by ScreenManager.Initialize, so we initialize it manually
            if (_isInitialised)
            {
                s.Initialize();
                s.LoadContent();
                s.Input = _input;
            }
            _screens.Add(s);
        }

        public void RemoveScreen(Screen s)
        {
            s.UnloadContent();
            _screens.Remove(s);
        }

        public SpriteBatchExtended SpriteBatch
        {
            get { return _spriteBatch; }
        }

        public SpriteFont Font
        {
            get { return _font; }
        }

        public SpriteFont NormalFont
        {
            get { return _normalFont; }
        }

        public List<Screen> Screens
        {
            get { return _screens; }
        }
        public ContentManager Content
        {
            get { return _content; }
        }
    }
}
