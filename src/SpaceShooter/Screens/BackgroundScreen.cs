﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;

namespace SpaceShooter
{
    //Basic background image and music
    class BackgroundScreen : Screen
    {
        private Texture2D _backgroundTexture;
        private Song _backgroundSong;

        public BackgroundScreen()
        {
            ScreenState = ScreenState.Covered;
        }

        public override void LoadContent()
        {
            _backgroundTexture = ScreenManager.Game.Content.Load<Texture2D>("Textures/background");
            _backgroundSong = ScreenManager.Game.Content.Load<Song>("Sounds/backgroundsong");
            MediaPlayer.Play(_backgroundSong);
        }

        public override void Update(GameTime gameTime)
        {
            

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch  = ScreenManager.SpriteBatch;
            GraphicsDevice gd = ScreenManager.GraphicsDevice;
            Viewport viewport = gd.Viewport;

            spriteBatch.Begin();
            spriteBatch.Draw(_backgroundTexture, new Rectangle(0, 0, viewport.Width, viewport.Height), Color.White);
            spriteBatch.End();

            gd.BlendState = BlendState.Opaque;
            gd.DepthStencilState = DepthStencilState.Default;
            gd.SamplerStates[0] = SamplerState.LinearWrap;
        }
    }
}
