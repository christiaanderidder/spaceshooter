﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Input;

namespace SpaceShooter
{
    class JoinScreen : Screen
    {
        private SignedInGamer[] _joinedPlayers = new SignedInGamer[4];
        private Viewport[] _viewports = new Viewport[4];
        private String[] _joinMessage = new String[4];
        private Viewport _fullViewport;
        private NetworkState _networkstate;
        private Texture2D _splitscreenTexture;
        private Boolean _isPlayer;

        public override void  LoadContent()
        {
            SignedInGamer.SignedIn += gamerSignedInEventHandler;
            _splitscreenTexture = ScreenManager.Content.Load<Texture2D>("Textures/splitscreen");
            _fullViewport = ScreenManager.GraphicsDevice.Viewport;

            //Set viewports
            _viewports[0] = new Viewport(0, 0, _fullViewport.Width / 2, _fullViewport.Height / 2);
            _viewports[1] = new Viewport(_fullViewport.Width / 2, 0, _fullViewport.Width / 2, _fullViewport.Height / 2);
            _viewports[2] = new Viewport(0, _fullViewport.Height / 2, _fullViewport.Width / 2, _fullViewport.Height / 2);
            _viewports[3] = new Viewport(_fullViewport.Width / 2, _fullViewport.Height / 2, _fullViewport.Width / 2, _fullViewport.Height / 2);
 	        
            base.LoadContent();
        }

        public override void HandleInput(GameTime gameTime)
        {
            for (int i = 0; i < 4; i++)
            {
                //Check if login guide is already visible
                if (!Guide.IsVisible)
                {
                    SignedInGamer g = Gamer.SignedInGamers[(PlayerIndex)i];

                    //Check if the Controlling player is signed in
                    if (g == null)
                    {
                        //Controlling player is not signed in yet, show sign in
                        if (Input.GamePad(Buttons.X, (PlayerIndex)i) || Input.Keyboard(Keys.X))
                        {
                            Guide.ShowSignIn(1, false);
                            //If sign in is finished SignedInGamer.SignedIn event is fired
                        }
                    }
                    else
                    {
                        //Controlling player is signed in check input
                        if (Input.GamePad(Buttons.X, (PlayerIndex)i))
                        {
                            if (_joinedPlayers[i] != null)
                            {
                                //The controlling player has already joined, so he will unjoin
                                _joinedPlayers[i] = null;
                            }
                            else
                            {
                                //The controlling player hasn't joined jet, so he will join
                                _joinedPlayers[i] = g;
                            }
                        }

                    }

                    if (_joinedPlayers[i] == null)
                    {
                        //player has not yet joined
                        _joinMessage[i] = "Press [X] to join";
                    }
                    else
                    {
                        //player is signed in and has joined
                        _joinMessage[i] = _joinedPlayers[i].Gamertag + " (Press [X] to cancel)";
                    }
                }

            }

            //Set NetworkState (this can only be changed by the first gamepad)
            if (Input.GamePad(Buttons.Y, PlayerIndex.One) || Input.Keyboard(Keys.Y))
            {
                if (_networkstate == NetworkState.Host)
                {
                    _networkstate = NetworkState.Client;
                }
                else if (_networkstate == NetworkState.Client)
                {
                    _networkstate = NetworkState.Host;
                }
            }
            
            //Go back to previous screen
            if (Input.MenuBack())
            {
                ScreenManager.PreviousScreen();
            }

            //Start Game
            if (Input.MenuSubmit())
            {
                //Check if at least one player has joined the game
                _isPlayer = false;
                foreach (SignedInGamer sig in _joinedPlayers)
                {
                    if (sig != null)
                    {
                        _isPlayer = true;
                    }
                }

                if (_isPlayer)
                {
                    //Copy joined players to list to pass on to the networksession, filtering null values
                    List<SignedInGamer> j = new List<SignedInGamer>();
                    foreach (SignedInGamer joinedPlayer in _joinedPlayers)
                    {
                        if (joinedPlayer != null)
                        {
                            j.Add(joinedPlayer);
                        }
                    }

                    //Add NetworkGameScreen to the screenmanager
                    ScreenManager.AddScreen(new NetworkGameScreen(ScreenManager.Game, j, _networkstate));

                    //Set screen to inactive, we do not remove it yet because this allows us to use the back button and return to this screen
                    ScreenState = ScreenState.Inactive;
                }
            }

            base.HandleInput(gameTime);
        }

        //Handle SignedInGamer.SignedIn event.
        private void gamerSignedInEventHandler(object sender, SignedInEventArgs e)
        {
            Console.WriteLine("SignedIn Fired");
            //Add SignedInGamer directly to joined players after sign in is completed
            _joinedPlayers[(int)e.Gamer.PlayerIndex] = e.Gamer;
        }

        public override void Draw(GameTime gameTime)
        {
            //Check if at least one player has joined the game
            _isPlayer = false;
            foreach (SignedInGamer sig in _joinedPlayers)
            {
                if (sig != null)
                {
                    _isPlayer = true;
                }
            }

            for (int i = 0; i < 4; i++)
            {

                Viewport v = _viewports[i];
                ScreenManager.SpriteBatch.Begin();

                //set active viewport
                ScreenManager.GraphicsDevice.Viewport = v;

                ScreenManager.SpriteBatch.Draw(_splitscreenTexture, new Vector2(15, 15), Color.White);
                Vector2 textdim = ScreenManager.NormalFont.MeasureString(_joinMessage[i]);
                ScreenManager.SpriteBatch.DrawGlyphString(ScreenManager.NormalFont, _joinMessage[i], new Vector2(v.Width / 2 - textdim.X / 2, 100f), Color.White);

                if (i == 0 && _isPlayer)
                {
                    string enterGameMessage = "Press [start] to enter combat\n";
                    Vector2 messageWidth = ScreenManager.NormalFont.MeasureString(enterGameMessage);
                    ScreenManager.SpriteBatch.DrawGlyphString(ScreenManager.NormalFont, enterGameMessage, new Vector2(v.Width / 2 - messageWidth.X / 2, 150f), Color.White);
                    enterGameMessage = _networkstate + " (Press [y] to change)";
                    messageWidth = ScreenManager.NormalFont.MeasureString(enterGameMessage);
                    ScreenManager.SpriteBatch.DrawGlyphString(ScreenManager.NormalFont, enterGameMessage, new Vector2(v.Width / 2 - messageWidth.X / 2, 200f), Color.White);
                }

                ScreenManager.SpriteBatch.End();
            }

            ScreenManager.GraphicsDevice.Viewport = _fullViewport;
            base.Draw(gameTime);
        }

    }
}
