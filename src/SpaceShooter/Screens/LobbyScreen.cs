﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Input;
using SpaceShooter.Screens;
using System.Diagnostics;
using System.Xml.Linq;

namespace SpaceShooter
{
    public class LobbyScreen : Screen
    {
        private Game _game;
        private NetworkSession _networkSession;
        private string _errorMessage;
        private PacketWriter _packetWriter = new PacketWriter();
        private PacketReader _packetReader = new PacketReader();
        private Viewport _fullviewport;
        private NetworkState _networkState;
        private Model _shipModel;
        private Model _skyboxModel;
        private Model _asteroidModel;
        private SpriteBatch _spriteBatch;
        private List<Asteroid> _asteroids;

        List<SignedInGamer> _joinedPlayers = new List<SignedInGamer>();

        //game stuff continue from here


        public LobbyScreen(Game game, List<SignedInGamer> joinedPlayers, NetworkState networkState)
        {
            _joinedPlayers = joinedPlayers;
            _game = game;
            _networkState = networkState;
            //_networkState = NetworkState.Client;
        }

        public override void Initialize()
        {
            _shipModel = ScreenManager.Content.Load<Model>("Models/ship");
            _skyboxModel = ScreenManager.Content.Load<Model>("Models/skybox2");
            _asteroidModel = ScreenManager.Content.Load<Model>("Models/asteroid1");

            _asteroids = new List<Asteroid>();
            _spriteBatch = new SpriteBatch(_game.GraphicsDevice);

            _fullviewport = ScreenManager.GraphicsDevice.Viewport;
            if (_networkState == NetworkState.Client)
            {
                JoinSession();
            }
            else if (_networkState == NetworkState.Host)
            {
                CreateSession();
            }
        }

        #region NetworkSession

        private void CreateSession()
        {
            try
            {
                _networkSession = NetworkSession.Create(NetworkSessionType.SystemLink, _joinedPlayers, 31, 0, new NetworkSessionProperties());
                setupGame();
                HookSessionEvents();
            }
            catch (Exception e)
            {
                _errorMessage = e.Message;
            }
        }

        private void JoinSession()
        {
            try
            {
                // Search for sessions.
                using (AvailableNetworkSessionCollection availableSessions = NetworkSession.Find(NetworkSessionType.SystemLink, _joinedPlayers, new NetworkSessionProperties()))
                {
                    if (availableSessions.Count == 0)
                    {
                        _errorMessage = "No network sessions found.";
                        Debug.WriteLine(_errorMessage);
                        return;
                    }

                    // Join the first session we found.
                    try
                    {
                        _networkSession = NetworkSession.Join(availableSessions[0]);

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    foreach (LocalNetworkGamer g in _networkSession.LocalGamers)
                    {
                        gamerJoinedEventHandler(_networkSession, new GamerJoinedEventArgs(g));
                    }
                    HookSessionEvents();
                }
            }
            catch (Exception e)
            {
                _errorMessage = e.Message;
            }
        }

        /// <summary>
        /// After creating or joining a network session, we must subscribe to
        /// some events so we will be notified when the session changes state.
        /// </summary>
        private void HookSessionEvents()
        {
            _networkSession.GamerJoined += gamerJoinedEventHandler;
            _networkSession.SessionEnded += sessionEndedEventHandler;
        }

        /// <summary>
        /// This event handler will be called whenever a new gamer joins the session.
        /// We use it to allocate a Tank object, and associate it with the new gamer.
        /// </summary>
        private void gamerJoinedEventHandler(object sender, GamerJoinedEventArgs e)
        {
            //new player joins, store player in NetworkGamer.Tag
            if (e.Gamer.IsLocal)
            {
                Debug.WriteLine("Player created: " + e.Gamer.Gamertag);
                LocalPlayer localPlayer = new LocalPlayer(_game);
                localPlayer.Ship.ShipModel = _shipModel;
                localPlayer.Ship.Input = Input;
                localPlayer.SkyBox.Model = _skyboxModel;
                localPlayer.Name = e.Gamer.Gamertag;
                e.Gamer.Tag = localPlayer;
                SetViewports();
            }
            else
            {
                Debug.WriteLine("Remoteplayer created: " + e.Gamer.Gamertag);
                Player player = new Player(_game);
                player.Ship.ShipModel = _shipModel;
                player.Name = e.Gamer.Gamertag;
                e.Gamer.Tag = player;
            }
        }

        /// <summary>
        /// Event handler notifies us when the network session has ended.
        /// </summary>
        private void sessionEndedEventHandler(object sender, NetworkSessionEndedEventArgs e)
        {
            _errorMessage = e.EndReason.ToString();
            Console.WriteLine(_errorMessage);
            _networkSession.Dispose();
        }

        public override void Update(GameTime gameTime)
        {
            if (_networkSession.IsDisposed)
            {
                //ScreenManager.AddScreen(new MenuScreen());
                Debug.WriteLine("Networksession disposed");
                ScreenManager.RemoveScreen(this);
            }
            else
            {
                UpdateNetworkSession();
            }
        }

        private void UpdateNetworkSession()
        {
            //if machine is client machine
            if (!_networkSession.IsHost)
            {
                ClientSendDataToHost();
                ClientReceiveDataFromHost();
                UpdateClient();
            }
            else if (_networkSession.IsHost)
            {
                HostReceiveDataFromClient();
                UpdateHost();
                HostSendDataToClient();
            }

            // Update the underlying session object.
            _networkSession.Update();

            // Make sure the session has not ended.
            if (_networkSession == null)
            {
                return;
            }

        }

        private void SetViewports()
        {
            int numberOfPlayers = _networkSession.LocalGamers.Count;
            for (int i = 0; i < numberOfPlayers; i++)
            {
                LocalPlayer p = _networkSession.LocalGamers[i].Tag as LocalPlayer;
                if (p != null)
                {
                    Viewport viewport = _fullviewport;

                    if (numberOfPlayers == 2)
                    {
                        viewport.Width /= 2;
                        viewport.X = viewport.Width * i;
                    }
                    else if (numberOfPlayers > 2)
                    {
                        viewport.Width /= 2;
                        viewport.Height /= 2;
                        if (i == 1 || i == 3)
                        {
                            viewport.X = viewport.Width;
                        }

                        if (i > 1)
                        {
                            viewport.Y = viewport.Height;
                        }
                    }
                    p.Viewport = viewport;
                }
            }
        }

        #endregion NetworkSession

        #region Common

        public override void HandleInput(GameTime gameTime)
        {
            if (Input.MenuBack())
            {
                ScreenManager.PreviousScreen();
                _networkSession.Dispose();
            }
            foreach (LocalNetworkGamer g in _networkSession.LocalGamers)
            {
                LocalPlayer localPlayer = g.Tag as LocalPlayer;
                localPlayer.PlayerIndex = g.SignedInGamer.PlayerIndex;
                localPlayer.HandleInput();
                localPlayer.Update();
            }

            
        }

        public override void Draw(GameTime gameTime)
        {
            _game.GraphicsDevice.Clear(Color.Black);
            Viewport full = ScreenManager.GraphicsDevice.Viewport;

            foreach (LocalNetworkGamer lg in _networkSession.LocalGamers)
            {
                LocalPlayer lp = lg.Tag as LocalPlayer;
                _game.GraphicsDevice.Viewport = lp.Viewport;

                lp.Ship.Draw(lp.ChaseCamera);
                lp.SkyBox.Draw(lp.ChaseCamera);
                foreach (Asteroid asteroid in _asteroids)
                {
                    asteroid.Draw(lp.ChaseCamera);
                }

                foreach (NetworkGamer g in _networkSession.AllGamers)
                {
                    Player p = g.Tag as Player;
                    if (p.Ship != lp.Ship)
                    {
                        Boolean isHost = false;
                        if (g.IsHost)
                        {
                            isHost = true;
                        }
                        p.Ship.Draw(lp.ChaseCamera);              

                        DrawGamerTag(lp, p, isHost);
                    }
                }
            }

            _game.GraphicsDevice.Viewport = full;
        }

        private void DrawGamerTag(LocalPlayer player, Player otherPlayer, Boolean isHost)
        {
            // distance 10000 is ver mis max voor total distance voor minimale weergave van naam, morgelijk 50 % van het normale? minimale scale 50 %.
            float scale;

            if (Vector3.Distance(player.Ship.Position, otherPlayer.Ship.Position) > 30000)
            {
                scale = 0.5f;
            }
            else if (Vector3.Distance(player.Ship.Position, otherPlayer.Ship.Position) > 2000)
            {
                scale = ((28000f - (Vector3.Distance(player.Ship.Position, otherPlayer.Ship.Position) - 2000f)) / 28000f) * 0.5f + 0.5f;
            }
            else
            {
                scale = 1;
            }

            _spriteBatch.Begin();


            Vector3 project = _game.GraphicsDevice.Viewport.Project(otherPlayer.Ship.Position, player.ChaseCamera.Projection, player.ChaseCamera.View, Matrix.Identity);
            if (project.Z < 1f)
            {
                string name;
                if (isHost)
                {
                    name = "*Host*" + otherPlayer.Name;
                }
                else
                {
                    name = otherPlayer.Name;
                }
                Vector2 stringSize = ScreenManager.NormalFont.MeasureString(name);
                Vector2 offSet = new Vector2(stringSize.X / 2, 0);


                project.X -= ScreenManager.GraphicsDevice.Viewport.X;
                project.Y -= 50f * scale + ScreenManager.GraphicsDevice.Viewport.Y;
                _spriteBatch.DrawString(ScreenManager.NormalFont, name, new Vector2(project.X, project.Y - 10 * scale), Color.Red, 0, offSet, scale, SpriteEffects.None, 1);
                _spriteBatch.DrawString(ScreenManager.NormalFont, "|", new Vector2(project.X, project.Y + 10 * scale), Color.Red, 0, new Vector2(0, 0), scale, SpriteEffects.None, 1);
            }
            _spriteBatch.End();

            _game.GraphicsDevice.BlendState = BlendState.Opaque;
            _game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            _game.GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
        }

        #endregion

        #region Client

        private void ClientSendDataToHost()
        {
            foreach (LocalNetworkGamer g in _networkSession.LocalGamers)
            {
                LocalPlayer localPlayer = g.Tag as LocalPlayer;
                _packetWriter.Write(g.Id);
                _packetWriter.Write(localPlayer.Ship.Position);
                _packetWriter.Write(localPlayer.Ship.Rotation);
                g.SendData(_packetWriter, SendDataOptions.InOrder, _networkSession.Host);
            }

        }

        private void ClientReceiveDataFromHost()
        {
            LocalNetworkGamer client = _networkSession.LocalGamers.First<LocalNetworkGamer>();
            //check if data is available
            while (client.IsDataAvailable)
            {
                //store sender
                NetworkGamer sender;
                client.ReceiveData(_packetReader, out sender);

               
                // This packet contains data about all the players in the session.
                // We keep reading from it until we have processed all the data.
                while (_packetReader.Position < _packetReader.Length)
                {
                    // Read the state of one player from the network packet.
                    byte gamerId = _packetReader.ReadByte();
                    Vector3 position = _packetReader.ReadVector3();
                    Quaternion rotation = _packetReader.ReadQuaternion();
                    String lvlData = _packetReader.ReadString();

                    // Look up which gamer this state refers to.
                    NetworkGamer remoteGamer = _networkSession.FindGamerById(gamerId);

                    // This might come back null if the gamer left the session after
                    // the host sent the packet but before we received it. If that
                    // happens, we just ignore the data for this gamer.
                    if (remoteGamer != null && !remoteGamer.IsLocal)
                    {
                        // Update our local state with data from the network packet.
                        Player remotePlayer = remoteGamer.Tag as Player;
                        remotePlayer.Ship.Position = position;
                        remotePlayer.Ship.Rotation = rotation;
                        readXML(lvlData);
                    }

                }

            }

        }

        private void readXML(string xmlString)
        {
            XDocument lvlData = XDocument.Parse(xmlString);
            _asteroids.Clear();
            Console.WriteLine();

                foreach (XElement e in lvlData.Element("asteroids").Elements())
                {
                    float posx = float.Parse(e.Element("positionX").Value);
                    float posy = float.Parse(e.Element("positionY").Value);
                    float posz = float.Parse(e.Element("positionZ").Value);

                    Vector3 pos = new Vector3(posx, posy, posz);

                    Random r = new Random();
                    Asteroid asteroid = new Asteroid(ScreenManager.Game, pos, r, 5);
                    _asteroids.Add(asteroid);

            }
        }

        private void UpdateClient()
        {
            foreach (NetworkGamer g in _networkSession.RemoteGamers)
            {
                Player p = g.Tag as Player;
                p.Ship.Update();
            }
        }

        #endregion Client

        #region Host

        private void HostReceiveDataFromClient()
        {
            LocalNetworkGamer server = _networkSession.Host as LocalNetworkGamer;
            //check if data is available
            while (server.IsDataAvailable)
            {
                //store sender
                NetworkGamer sender;
                server.ReceiveData(_packetReader, out sender);
                if (sender.Machine != server.Machine)
                {

                    // This packet contains data about all the players in the session.
                    // We keep reading from it until we have processed all the data.
                    while (_packetReader.Position < _packetReader.Length)
                    {
                        // Read the state of one player from the network packet.
                        byte gamerId = _packetReader.ReadByte();
                        Vector3 position = _packetReader.ReadVector3();
                        Quaternion rotation = _packetReader.ReadQuaternion();


                        // Look up which gamer this state refers to.
                        NetworkGamer remoteGamer = _networkSession.FindGamerById(gamerId);

                        // This might come back null if the gamer left the session after
                        // the host sent the packet but before we received it. If that
                        // happens, we just ignore the data for this gamer.
                        if (remoteGamer != null && !remoteGamer.IsLocal)
                        {
                            // Update our local state with data from the network packet.
                            Player remotePlayer = remoteGamer.Tag as Player;
                            remotePlayer.Ship.Position = position;
                            remotePlayer.Ship.Rotation = rotation;
                        }

                    }
                }

            }
        }

        private void HostSendDataToClient()
        {
            if (_networkSession.RemoteGamers.Count > 0)
            {
            _packetWriter.Flush();
            //setup gamestate from player data
            string lvlData = convertLevelDataToXML();
            
                foreach (NetworkGamer g in _networkSession.AllGamers)
                {
                    Player p = g.Tag as Player;
                    _packetWriter.Write(g.Id);
                    _packetWriter.Write(p.Ship.Position);
                    _packetWriter.Write(p.Ship.Rotation);
                    _packetWriter.Write(lvlData);
                }
                // Send gamestate to players
                LocalNetworkGamer server = _networkSession.Host as LocalNetworkGamer;
                if (_packetWriter.Length > 0)
                {
                    server.SendData(_packetWriter, SendDataOptions.InOrder);
                }
            }
        }

        private void UpdateHost()
        {
            foreach (NetworkGamer g in _networkSession.RemoteGamers)
            {
                Player p = g.Tag as Player;
                p.Ship.Update();
            }

            foreach (Asteroid asteroid in _asteroids)
            {
                asteroid.Update();
            }
        }

        private void setupGame()
        {
            Random r = new Random();
            addAsteroids(25, 0.5f, 1.25f, r);
            addAsteroids(15, 4.5f, 7.5f, r);
            addAsteroids(7, 10f, 12.5f, r);
        }

        
        private string convertLevelDataToXML()
        {
            string xmlString;
            xmlString = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
            xmlString += "<data>";
            xmlString += "<asteroids>";
            int i = 0;
            foreach (Asteroid asteroid in _asteroids)
            {
                xmlString += "<asteroid>";
                xmlString += "<id>" + i.ToString() + "</id>";
                xmlString += "<positionX>" + asteroid.Position.X +  "</positionX>";
                xmlString += "<positionY>" + asteroid.Position.Y +"</positionY>";
                xmlString += "<positionZ>" + asteroid.Position.Z +"</positionZ>";
                xmlString += "<rotation>" + asteroid.Rotation.ToString() +"</rotation>";
                xmlString += "<modelRotation>" + asteroid.ModelRotation.ToString() + "</modelRotation>";
                xmlString += "<velocity>" + asteroid.Velocity.ToString() +"</velocity>";
                xmlString += "</asteroid>";
                i++;
            }
            xmlString += "</asteroids>";
            xmlString += "</data>";

            return xmlString;
        }

        private void addAsteroids(int amount, float minScale, float maxScale, Random r)
        {
            float x;
            float y;
            float z;
            float scale;

            for (int i = 0; i < amount; i++)
            {
                x = r.Next(-150000, 150000);
                y = r.Next(-150000, 150000);
                z = r.Next(-150000, 150000);
                scale = (float)r.NextDouble() * (maxScale - minScale) + minScale;
                Vector3 startLoc = new Vector3(x, y, z);
                Asteroid asteroid = new Asteroid(_game, startLoc, r, scale);
                asteroid.Mass = 250;
                asteroid.Model = _asteroidModel;

                _asteroids.Add(asteroid);
            }

        }
        #endregion Host
    }

}