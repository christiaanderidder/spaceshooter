﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace SpaceShooter
{
    class MainMenuScreen : MenuScreen
    {

        public MainMenuScreen()
        {
            ItemTextColor = new Color(49,105,152);
            ItemSelectedTextColor = new Color(49, 105, 152);


            MenuItem playItem = new MenuItem("play");
            MenuItem exitItem = new MenuItem("exit");

            playItem.Pressed += new EventHandler(playItem_Pressed);
            exitItem.Pressed += new EventHandler(exitItem_Pressed);

            MenuItems.Add(playItem);
            MenuItems.Add(exitItem);
        }

        public override void LoadContent()
        {
            Content = new ContentManager(ScreenManager.Game.Services, "Content");
            ItemSound = Content.Load<SoundEffect>("Sounds/menu");
            ItemTexture = Content.Load<Texture2D>("Textures/menuitem");
            ItemSelectedTexture = Content.Load<Texture2D>("Textures/selectedmenuitem");
        }

        void playItem_Pressed(object sender, EventArgs e)
        {
            Console.WriteLine("Play is pressed");
            ScreenManager.AddScreen(new JoinScreen());
            ScreenState = ScreenState.Inactive;
        }

        void exitItem_Pressed(object sender, EventArgs e)
        {
            Console.WriteLine("Exit is pressed");
            ScreenManager.Game.Exit();
        }

    }
}
