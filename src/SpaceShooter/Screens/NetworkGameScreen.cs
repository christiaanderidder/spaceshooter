﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;
using System.Xml.Linq;

namespace SpaceShooter
{

    public class NetworkGameScreen : Screen
    {
        private Game _game;
        private NetworkSession _networkSession;
        private string _errorMessage;
        private PacketWriter _packetWriter = new PacketWriter();
        private PacketReader _packetReader = new PacketReader();
        private Viewport _fullviewport;
        private NetworkState _networkState;
       
        private SpriteBatch _spriteBatch;
        private List<Asteroid> _asteroids;
        private List<Vector3> _respawnPoints;
        private int _nextSpawnPoint;

        List<SignedInGamer> _joinedPlayers = new List<SignedInGamer>();

        //models and textures

        private Texture2D _barBgTexture;
        private Texture2D _barFillTexture;
        private Model _shipModel;
        private Model _skyboxModel;
        private Model _asteroidModel;

        

        //game stuff continue from here


        public NetworkGameScreen(Game game, List<SignedInGamer> joinedPlayers, NetworkState networkState)
        {
            _joinedPlayers = joinedPlayers;
            _game = game;
            _networkState = networkState;
        }

        public override void Initialize()
        {
            Debug.WriteLine("init called");
            _shipModel = ScreenManager.Content.Load<Model>("Models/ship");
            _skyboxModel = ScreenManager.Content.Load<Model>("Models/skybox2");
            _asteroidModel = ScreenManager.Content.Load<Model>("Models/asteroid1");
            _barBgTexture = ScreenManager.Content.Load<Texture2D>("Textures/barbg");
            _barFillTexture = ScreenManager.Content.Load<Texture2D>("Textures/barfill");

            _asteroids = new List<Asteroid>();
            _spriteBatch = new SpriteBatch(_game.GraphicsDevice);
            addSpawnPoints();

            _fullviewport = ScreenManager.GraphicsDevice.Viewport;
            if (_networkState == NetworkState.Client)
            {
                JoinSession();
            }
            else if (_networkState == NetworkState.Host)
            {
                CreateSession();
            }
        }

        #region NetworkSession

        private void CreateSession()
        {
            Debug.WriteLine("Create called");
            try
            {
                _networkSession = NetworkSession.Create(NetworkSessionType.SystemLink, _joinedPlayers, 31, 0, new NetworkSessionProperties());
                HookSessionEvents();
                setupGame();
            }
            catch (Exception e)
            {
                _errorMessage = e.Message;
            }
        }

        private void JoinSession()
        {
            try
            {
                // Search for sessions.
                using (AvailableNetworkSessionCollection availableSessions = NetworkSession.Find(NetworkSessionType.SystemLink, _joinedPlayers, new NetworkSessionProperties()))
                {
                    if (availableSessions.Count == 0)
                    {
                        _errorMessage = "No network sessions found.";
                        Debug.WriteLine(_errorMessage);
                        return;
                    }

                    // Join the first session we found.
                    try
                    {
                        _networkSession = NetworkSession.Join(availableSessions[0]);
                        HookSessionEvents();
                    }
                    catch (Exception e)
                    {

                        Console.WriteLine(e.Message);
                        ScreenManager.PreviousScreen();
                        _networkSession.Dispose();
                    }
                    
                }
            }
            catch (Exception e)
            {
                _errorMessage = e.Message;
            }
        }

        /// <summary>
        /// After creating or joining a network session, we must subscribe to
        /// some events so we will be notified when the session changes state.
        /// </summary>
        private void HookSessionEvents()
        {
            _networkSession.GamerJoined += gamerJoinedEventHandler;
            _networkSession.SessionEnded += sessionEndedEventHandler;
        }

        // This event handler will be called whenever a new gamer joins the session.
        private void gamerJoinedEventHandler(object sender, GamerJoinedEventArgs e)
        {
            //new player joins, store player in NetworkGamer.Tag
            if (e.Gamer.IsLocal)
            {
                //for a local player we will need to initialize some extra things
                Debug.WriteLine("Player created: " + e.Gamer.Gamertag);
                LocalPlayer localPlayer = new LocalPlayer(_game);
                localPlayer.Ship.ShipModel = _shipModel;
                localPlayer.Ship.Input = Input;
                localPlayer.SkyBox.Model = _skyboxModel;
                localPlayer.Name = e.Gamer.Gamertag;
                e.Gamer.Tag = localPlayer;
                SetViewports();
                localPlayer.Ship.Respawn(getSpawnPoint());
            }
            else
            {
                Debug.WriteLine("Remoteplayer created: " + e.Gamer.Gamertag);
                Player player = new Player(_game);
                player.Ship.ShipModel = _shipModel;
                player.Name = e.Gamer.Gamertag;
                e.Gamer.Tag = player;
            }
        }

        // Event handler notifies us when the network session has ended.
        private void sessionEndedEventHandler(object sender, NetworkSessionEndedEventArgs e)
        {
            _errorMessage = e.EndReason.ToString();
            Console.WriteLine(_errorMessage);
            _networkSession.Dispose();
        }

        public override void Update(GameTime gameTime)
        {
            if (_networkSession != null)
            {
                //Check if the networksession is disposed, if so remove this screen and return to the previous screen
                if (_networkSession.IsDisposed)
                {
                    Debug.WriteLine("Networksession disposed");
                    ScreenManager.PreviousScreen();
                }
                else
                {
                    UpdateNetworkSession();
                }
            }
        }

        private void UpdateNetworkSession()
        {
            //if machine is client machine
            if (!_networkSession.IsHost)
            {
                ClientSendDataToHost();
                ClientReceiveDataFromHost();
                UpdateClient();
            }
            else if (_networkSession.IsHost)
            {
                HostReceiveDataFromClient();
                UpdateHost();
                HostSendDataToClient();
            }

            checkForCollisions();
            // Update the underlying session object.
            _networkSession.Update();


            // Make sure the session has not ended.
            if (_networkSession == null)
            {
                return;
            }

        }

        //Set the number of viewports according to the number of players
        private void SetViewports()
        {
            int numberOfPlayers = _networkSession.LocalGamers.Count;
            for (int i = 0; i < numberOfPlayers; i++)
            {
                LocalPlayer p = _networkSession.LocalGamers[i].Tag as LocalPlayer;
                if (p != null)
                {
                    Viewport viewport = _fullviewport;

                    if (numberOfPlayers == 2)
                    {
                        viewport.Width /= 2;
                        viewport.X = viewport.Width * i;
                    }
                    else if (numberOfPlayers > 2)
                    {
                        viewport.Width /= 2;
                        viewport.Height /= 2;
                        if (i == 1 || i == 3)
                        {
                            viewport.X = viewport.Width;
                        }

                        if (i > 1)
                        {
                            viewport.Y = viewport.Height;
                        }
                    }
                    p.Viewport = viewport;
                }
            }
        }

        #endregion NetworkSession

        #region Common

        public override void HandleInput(GameTime gameTime)
        {
            if (Input.MenuBack())
            {
                ScreenManager.PreviousScreen();
                _networkSession.Dispose();
            }

            if (_networkSession != null)
            {
                foreach (LocalNetworkGamer g in _networkSession.LocalGamers)
                {
                    LocalPlayer localPlayer = g.Tag as LocalPlayer;
                    localPlayer.PlayerIndex = g.SignedInGamer.PlayerIndex;
                    localPlayer.HandleInput();
                    localPlayer.Update();
                    if (localPlayer.Ship.Health <= 0)
                    {
                        localPlayer.Ship.Respawn(getSpawnPoint());
                    }
                }
            }

            
        }

        public override void Draw(GameTime gameTime)
        {
            if (_networkSession != null)
            {
                _game.GraphicsDevice.Clear(Color.Black);
                Viewport full = ScreenManager.GraphicsDevice.Viewport;

                foreach (LocalNetworkGamer lg in _networkSession.LocalGamers)
                {
                    LocalPlayer lp = lg.Tag as LocalPlayer;
                    _game.GraphicsDevice.Viewport = lp.Viewport;

                    lp.Ship.Draw(lp.ChaseCamera);
                    lp.SkyBox.Draw(lp.ChaseCamera);
                    foreach (Asteroid asteroid in _asteroids)
                    {
                        asteroid.Draw(lp.ChaseCamera);
                    }

                    foreach (NetworkGamer g in _networkSession.AllGamers)
                    {
                        Player p = g.Tag as Player;
                        if (p.Ship != lp.Ship)
                        {
                            Boolean isHost = false;
                            if (g.IsHost)
                            {
                                isHost = true;
                            }
                            p.Ship.Draw(lp.ChaseCamera);

                            DrawGamerTag(lp, p, isHost);
                        }
                    }
                    DrawGamerStats(lp);
                }

                _game.GraphicsDevice.Viewport = full;
            }
        }

        private void DrawGamerTag(LocalPlayer player, Player otherPlayer, Boolean isHost)
        {
            // distance 10000 is ver mis max voor total distance voor minimale weergave van naam, morgelijk 50 % van het normale? minimale scale 50 %.
            float scale;

            if (Vector3.Distance(player.Ship.Position, otherPlayer.Ship.Position) > 30000)
            {
                scale = 0.5f;
            }
            else if (Vector3.Distance(player.Ship.Position, otherPlayer.Ship.Position) > 2000)
            {
                scale = ((28000f - (Vector3.Distance(player.Ship.Position, otherPlayer.Ship.Position) - 2000f)) / 28000f) * 0.5f + 0.5f;
            }
            else
            {
                scale = 1;
            }

            _spriteBatch.Begin();


            Vector3 project = _game.GraphicsDevice.Viewport.Project(otherPlayer.Ship.Position, player.ChaseCamera.Projection, player.ChaseCamera.View, Matrix.Identity);
            if (project.Z < 1f)
            {
                string name;
                if (isHost)
                {
                    name = "*Host*" + otherPlayer.Name;
                }
                else
                {
                    name = otherPlayer.Name;
                }
                Vector2 stringSize = ScreenManager.NormalFont.MeasureString(name);
                Vector2 offSet = new Vector2(stringSize.X / 2, 0);


                project.X -= ScreenManager.GraphicsDevice.Viewport.X;
                project.Y -= 50f * scale + ScreenManager.GraphicsDevice.Viewport.Y;
                _spriteBatch.DrawString(ScreenManager.NormalFont, name, new Vector2(project.X, project.Y - 10 * scale), Color.Red, 0, offSet, scale, SpriteEffects.None, 1);
                _spriteBatch.DrawString(ScreenManager.NormalFont, "|", new Vector2(project.X, project.Y + 10 * scale), Color.Red, 0, new Vector2(0, 0), scale, SpriteEffects.None, 1);
            }
            _spriteBatch.End();

            _game.GraphicsDevice.BlendState = BlendState.Opaque;
            _game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            _game.GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
        }

        private void DrawGamerStats(LocalPlayer player)
        {
            _spriteBatch.Begin();
            _spriteBatch.Draw(_barBgTexture, new Vector2(50, player.Viewport.Height - 50), Color.White);
            _spriteBatch.Draw(_barFillTexture, new Vector2(50, player.Viewport.Height - 50), Color.White);
            _spriteBatch.DrawString(ScreenManager.Font, "HEALTH: " + player.Ship.Health, new Vector2(50, 50), Color.White);
            _spriteBatch.DrawString(ScreenManager.Font, "ENERGY: " + Math.Round(player.Ship.Energy), new Vector2(50, 75), Color.White);
            _spriteBatch.End();

            _game.GraphicsDevice.BlendState = BlendState.Opaque;
            _game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            _game.GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
        }

        #endregion

        #region Client




        private void ClientSendDataToHost()
        {
            foreach (LocalNetworkGamer g in _networkSession.LocalGamers)
            {
                LocalPlayer localPlayer = g.Tag as LocalPlayer;
                Int32 velocity = (Int32)localPlayer.Ship.Velocity;

                
                _packetWriter.Write(g.Id);
                _packetWriter.Write(localPlayer.Ship.Position);
                _packetWriter.Write(localPlayer.Ship.Rotation);
                _packetWriter.Write(localPlayer.Ship.IsShooting);
                _packetWriter.Write(velocity);
                g.SendData(_packetWriter, SendDataOptions.InOrder, _networkSession.Host);
            }

        }

        private void ClientReceiveDataFromHost()
        {
            LocalNetworkGamer client = _networkSession.LocalGamers.First<LocalNetworkGamer>();
            //check if data is available
            while (client.IsDataAvailable)
            {
                //store sender
                NetworkGamer sender;
                client.ReceiveData(_packetReader, out sender);

               
                // This packet contains data about all the players in the session.
                // We keep reading from it until we have processed all the data.
                while (_packetReader.Position < _packetReader.Length)
                {
                    // Read the state of one player from the network packet.
                    byte gamerId = _packetReader.ReadByte();
                    Vector3 position = _packetReader.ReadVector3();
                    Quaternion rotation = _packetReader.ReadQuaternion();
                    Boolean isShooting = _packetReader.ReadBoolean();
                    Int32 velocity = _packetReader.ReadInt32();

                    // Look up which gamer this state refers to.
                    NetworkGamer remoteGamer = _networkSession.FindGamerById(gamerId);

                    // This might come back null if the gamer left the session after
                    // the host sent the packet but before we received it. If that
                    // happens, we just ignore the data for this gamer.
                    if (remoteGamer != null && !remoteGamer.IsLocal)
                    {
                        // Update our local state with data from the network packet.
                        Player remotePlayer = remoteGamer.Tag as Player;
                        remotePlayer.Ship.Position = position;
                        remotePlayer.Ship.Rotation = rotation;
                        remotePlayer.Ship.IsShooting = isShooting;
                        remotePlayer.Ship.Velocity = (float)velocity;
                    }

                }

            }

        }
        //reads out the xml string send by the host and converts it into asteroids
        private void readXML(string xmlString)
        {
            XDocument lvlData = XDocument.Parse(xmlString);
            _asteroids.Clear();
            

                foreach (XElement e in lvlData.Descendants("asteroid"))
                {
                    float posx = float.Parse(e.Element("positionX").Value);
                    float posy = float.Parse(e.Element("positionY").Value);
                    float posz = float.Parse(e.Element("positionZ").Value);
                    Vector3 pos = new Vector3(posx, posy, posz);

                    //regular Rotation
                    float rotX = float.Parse(e.Element("rotationX").Value);
                    float rotY = float.Parse(e.Element("rotationY").Value);
                    float rotZ = float.Parse(e.Element("rotationZ").Value);
                    float rotW = float.Parse(e.Element("rotationW").Value);
                    Quaternion rot = new Quaternion(rotX, rotY, rotZ, rotW);

                    //additional Rotation 
                    float adrotX = float.Parse(e.Element("additionalRotationX").Value);
                    float adrotY = float.Parse(e.Element("additionalRotationY").Value);
                    float adrotZ = float.Parse(e.Element("additionalRotationZ").Value);
                    float adrotW = float.Parse(e.Element("additionalRotationW").Value);
                    Quaternion adrot = new Quaternion(adrotX, adrotY, adrotZ, adrotW);

                    //modelRotation 
                    float mrotX = float.Parse(e.Element("modelRotationX").Value);
                    float mrotY = float.Parse(e.Element("modelRotationY").Value);
                    float mrotZ = float.Parse(e.Element("modelRotationZ").Value);
                    float mrotW = float.Parse(e.Element("modelRotationW").Value);
                    Quaternion modelRot = new Quaternion(mrotX, mrotY, mrotZ, mrotW);

                    float scale = float.Parse(e.Element("scale").Value);

                    Asteroid asteroid = new Asteroid(ScreenManager.Game, pos, scale);
                    asteroid.Model = _asteroidModel;
                    asteroid.Rotation = rot;
                    asteroid.AdditionalRotation = adrot;
                    asteroid.ModelRotation = modelRot;
                    asteroid.ModelJaw = float.Parse(e.Element("modelJaw").Value);
                    asteroid.ModelPitch = float.Parse(e.Element("modelPitch").Value);
                    asteroid.ModelRoll = float.Parse(e.Element("modelRoll").Value);

                    asteroid.MoveJaw = float.Parse(e.Element("moveJaw").Value);
                    asteroid.MovePitch = float.Parse(e.Element("movePitch").Value);
                    asteroid.MoveRoll = float.Parse(e.Element("moveRoll").Value);

                    asteroid.Velocity = float.Parse(e.Element("velocity").Value);
                    asteroid.AdditionalVelocity = float.Parse(e.Element("additionalVelocity").Value);

                    //Console.WriteLine(asteroid.AdditionalVelocity);


                    _asteroids.Add(asteroid);

            }
        }

        private void UpdateClient()
        {
            foreach (NetworkGamer g in _networkSession.RemoteGamers)
            {
                Player p = g.Tag as Player;
                p.Ship.Update();
            }
        }

        #endregion Client

        #region Host

        private void HostReceiveDataFromClient()
        {
            LocalNetworkGamer server = _networkSession.Host as LocalNetworkGamer;
            //check if data is available
            while (server.IsDataAvailable)
            {
                //store sender
                NetworkGamer sender;
                server.ReceiveData(_packetReader, out sender);
                if (sender.Machine != server.Machine)
                {
                    // This packet contains data about all the players in the session.
                    // We keep reading from it until we have processed all the data.
                    while (_packetReader.Position < _packetReader.Length)
                    {
                        // Read the state of one player from the network packet.
                        byte gamerId = _packetReader.ReadByte();
                        Vector3 position = _packetReader.ReadVector3();
                        Quaternion rotation = _packetReader.ReadQuaternion();
                        Boolean isShooting = _packetReader.ReadBoolean();
                        int velocity = _packetReader.ReadInt32();

                        // Look up which gamer this state refers to.
                        NetworkGamer remoteGamer = _networkSession.FindGamerById(gamerId);

                        // This might come back null if the gamer left the session after
                        // the host sent the packet but before we received it. If that
                        // happens, we just ignore the data for this gamer.
                        if (remoteGamer != null && !remoteGamer.IsLocal)
                        {
                            // Update our local state with data from the network packet.
                            Player remotePlayer = remoteGamer.Tag as Player;
                            remotePlayer.Ship.Position = position;
                            remotePlayer.Ship.Rotation = rotation;
                            remotePlayer.Ship.IsShooting = isShooting;
                            remotePlayer.Ship.Velocity = (float)velocity;
                        
                        }

                    }
                }

            }
        }

        private void HostSendDataToClient()
        {
            if (_networkSession.RemoteGamers.Count > 0)
            {
            _packetWriter.Flush();
            //setup gamestate from player data
            //string lvlData = convertLevelDataToXML();
            
                foreach (NetworkGamer g in _networkSession.AllGamers)
                {
                    Player p = g.Tag as Player;
                    Int32 velocity = (Int32)p.Ship.Velocity;

                    _packetWriter.Write(g.Id);
                    _packetWriter.Write(p.Ship.Position);
                    _packetWriter.Write(p.Ship.Rotation);
                    _packetWriter.Write(p.Ship.IsShooting);
                    _packetWriter.Write(velocity);
                }
                // Send gamestate to players
                LocalNetworkGamer server = _networkSession.Host as LocalNetworkGamer;
                if (_packetWriter.Length > 0)
                {
                    server.SendData(_packetWriter, SendDataOptions.InOrder);
                }
            }
        }

        private void UpdateHost()
        {
            foreach (NetworkGamer g in _networkSession.RemoteGamers)
            {
                Player p = g.Tag as Player;
                p.Ship.Update();
            }

            foreach (Asteroid asteroid in _asteroids)
            {
                asteroid.Update();
            }
        }

        private void setupGame()
        {
            Random r = new Random();
            addAsteroids(25, 0.5f, 1.25f, r);
            addAsteroids(15, 4.5f, 7.5f, r);
            addAsteroids(7, 10f, 12.5f, r);

            //readXML(convertLevelDataToXML());
        }

        //creates an xml string wich can be send to remote players, it contains all the data needed to add all asteroids that are currently in game of the host
        private string convertLevelDataToXML()
        {
            string xmlString;
            xmlString = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
            xmlString += "<data>";
            xmlString += "<asteroids>";
            int i = 0;
            foreach (Asteroid asteroid in _asteroids)
            {
                xmlString += "<asteroid>";
                xmlString += "<id>" + i.ToString() + "</id>";
                xmlString += "<positionX>" + asteroid.Position.X +  "</positionX>";
                xmlString += "<positionY>" + asteroid.Position.Y +"</positionY>";
                xmlString += "<positionZ>" + asteroid.Position.Z +"</positionZ>";

                xmlString += "<rotationW>" + asteroid.Rotation.W + "</rotationW>";
                xmlString += "<rotationX>" + asteroid.Rotation.X + "</rotationX>";
                xmlString += "<rotationY>" + asteroid.Rotation.Y + "</rotationY>";
                xmlString += "<rotationZ>" + asteroid.Rotation.Z + "</rotationZ>";

                xmlString += "<modelRotationW>" + asteroid.ModelRotation.W + "</modelRotationW>";
                xmlString += "<modelRotationX>" + asteroid.ModelRotation.X + "</modelRotationX>";
                xmlString += "<modelRotationY>" + asteroid.ModelRotation.Y + "</modelRotationY>";
                xmlString += "<modelRotationZ>" + asteroid.ModelRotation.Z + "</modelRotationZ>";

                xmlString += "<additionalRotationW>" + asteroid.AdditionalRotation.W + "</additionalRotationW>";
                xmlString += "<additionalRotationX>" + asteroid.AdditionalRotation.X + "</additionalRotationX>";
                xmlString += "<additionalRotationY>" + asteroid.AdditionalRotation.Y + "</additionalRotationY>";
                xmlString += "<additionalRotationZ>" + asteroid.AdditionalRotation.Z + "</additionalRotationZ>";

                xmlString += "<modelJaw>" + asteroid.ModelJaw + "</modelJaw>";
                xmlString += "<modelPitch>" + asteroid.ModelPitch + "</modelPitch>";
                xmlString += "<modelRoll>" + asteroid.ModelRoll + "</modelRoll>";

                xmlString += "<moveJaw>" + asteroid.MoveJaw + "</moveJaw>";
                xmlString += "<movePitch>" + asteroid.MovePitch + "</movePitch>";
                xmlString += "<moveRoll>" + asteroid.MoveRoll + "</moveRoll>";

                xmlString += "<velocity>" + asteroid.Velocity +"</velocity>";
                xmlString += "<additionalVelocity>" + asteroid.AdditionalVelocity + "</additionalVelocity>";
                xmlString += "<scale>" + asteroid.Scale + "</scale>";
                xmlString += "</asteroid>";
                i++;
            }
            xmlString += "</asteroids>";
            xmlString += "</data>";

            return xmlString;
        }

        //sets the asteroids of the host
        private void addAsteroids(int amount, float minScale, float maxScale, Random r)
        {
            float x;
            float y;
            float z;
            float scale;

            for (int i = 0; i < amount; i++)
            {
                x = r.Next(-150000, 150000);
                y = r.Next(-150000, 150000);
                z = r.Next(-150000, 150000);
                scale = (float)r.NextDouble() * (maxScale - minScale) + minScale;
                Vector3 startLoc = new Vector3(x, y, z);
                Asteroid asteroid = new Asteroid(_game, startLoc, scale);

                asteroid.MoveJaw = (float)r.NextDouble() * 0.01f - 0.005f;
                asteroid.ModelJaw = (float)r.NextDouble() * 0.01f - 0.005f;
                asteroid.MovePitch = (float)r.NextDouble() * 0.01f - 0.005f;
                asteroid.ModelPitch = (float)r.NextDouble() * 0.01f - 0.005f;
                asteroid.MoveRoll = (float)r.NextDouble() * 0.01f - 0.005f;
                asteroid.ModelRoll = (float)r.NextDouble() * 0.01f - 0.005f;
                asteroid.Velocity = r.Next(30, 45);
                asteroid.Mass = 250;
                asteroid.Model = _asteroidModel;

                _asteroids.Add(asteroid);
            }
        }
        #endregion Host

        #region collision

        //loops through astroids and ships to check for collisions
        protected void checkForCollisions()
        {
            foreach (NetworkGamer g in _networkSession.AllGamers)
            {
                bool collided = false;
                Player p1 = g.Tag as Player;
                foreach (Asteroid asteroid in _asteroids)
                {
                    bool asteroidCollided = false;
                    if (Vector3.Distance(p1.Ship.Position, asteroid.Position) < (asteroid.Scale * 3000f))
                    {
                        collided = checkObjectCollision(p1.Ship, asteroid);
                        asteroidCollided = collided;
                    }
                    checkForMissilecollisions(asteroid, p1.Ship);                  
                    if (asteroidCollided == false)
                    {
                        asteroid.IsColliding = false;
                    }
                }

                foreach (NetworkGamer g2 in _networkSession.AllGamers)
                {
                    Player p2 = g2.Tag as Player;

                    if (p1 != p2)
                    {
                        if (Vector3.Distance(p1.Ship.Position, p2.Ship.Position) < 15000f)
                        {
                            collided = checkObjectCollision(p1.Ship, p2.Ship);
                        }
                        checkForMissilecollisions(p1.Ship, p2.Ship);
                    }
                }

                if (collided == false)
                {
                    p1.Ship.IsColliding = false;
                }
            }

            foreach (Asteroid asteroid in _asteroids)
            {
                if (asteroid.IsColliding == false)
                {
                    foreach (Asteroid otherAsteroid in _asteroids)
                    {
                        if (asteroid != otherAsteroid && Vector3.Distance(asteroid.Position, otherAsteroid.Position) < 10000f)
                        {
                            checkObjectCollision(asteroid, otherAsteroid);
                        }
                    }
                }

            }
        }

        //checks if 2 objects with the iCollidable interface are colliding by checking if any of the boundingspheres are intersecting
        private bool checkObjectCollision(ICollidable obj1, ICollidable obj2)
        {
            bool collided = false;
            if (obj1.IsColliding == false && obj2.IsColliding == false)
            {
                foreach (Bound obj1Bound in obj1.Bounds)
                {
                    foreach (Bound obj2Bound in obj2.Bounds)
                    {
                        if (obj1Bound.BoundingSphere.Intersects(obj2Bound.BoundingSphere))
                        {
                            float obj1CurrentVelocity = obj1.Velocity;
                            float obj2CurrentVelocity = obj2.Velocity;

                            obj1.Collide(obj2CurrentVelocity, obj2.Rotation, obj2.Mass);
                            obj2.Collide(obj1CurrentVelocity, obj1.Rotation, obj1.Mass);
                            obj1.IsColliding = true;
                            obj2.IsColliding = true;

                            collided = true;
                        }
                    }
                }
            }
            return collided;
        }
        //checks wether any of the ship's missiles are colliding with an icollidable object
        private void checkForMissilecollisions(ICollidable obj, Ship ship)
        {
            foreach (Missile missile in ship.Missiles)
            {
                if (Vector3.Distance(obj.Position, missile.Position) < 20000f)
                {
                    foreach (Bound objBound in obj.Bounds)
                    {
                        foreach (Bound missileBound in missile.Bounds)
                        {
                            if (objBound.BoundingSphere.Intersects(missileBound.BoundingSphere))
                            {
                                missile.Collide(obj);
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region respawning
        //sets all the respawn locations
        private void addSpawnPoints()
        {
            _nextSpawnPoint = 0;
            _respawnPoints = new List<Vector3>();
            Vector3 point1 = new Vector3(50000, 50000, 50000);
            Vector3 point2 = new Vector3(50000, -50000, 50000);
            Vector3 point3 = new Vector3(50000, 50000, -50000);
            Vector3 point4 = new Vector3(50000, -50000, -50000);
            Vector3 point5 = new Vector3(-50000, 50000, 50000);
            Vector3 point6 = new Vector3(-50000, 50000, -50000);
            Vector3 point7 = new Vector3(-50000, -50000, 50000);
            Vector3 point8 = new Vector3(-50000, -50000, -50000);
            Vector3 point9 = Vector3.Zero;

            _respawnPoints.Add(point1);
            _respawnPoints.Add(point2);
            _respawnPoints.Add(point3);
            _respawnPoints.Add(point4);
            _respawnPoints.Add(point5);
            _respawnPoints.Add(point6);
            _respawnPoints.Add(point7);
            _respawnPoints.Add(point8);
            _respawnPoints.Add(point9);
        }

        //secects the next spawn point from the spawn list. Used so a ship will not spawn on the same location it did before
        private Vector3 getSpawnPoint()
        {
            Vector3 point = _respawnPoints[_nextSpawnPoint];

            _nextSpawnPoint++;
            if (_nextSpawnPoint == _respawnPoints.Count)
            {
                _nextSpawnPoint = 0;
            }

            return point;
        }

        #endregion
    }

}