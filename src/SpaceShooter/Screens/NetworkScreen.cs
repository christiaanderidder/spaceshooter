﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;
using System.Xml.Linq;

namespace SpaceShooter
{
    public enum NetworkState
    {
        Host,
        Client
    };

    public class NetworkScreen : Screen
    {
        private Game _game;
        private NetworkSession _networkSession;
        private PacketWriter _packetWriter = new PacketWriter();
        private PacketReader _packetReader = new PacketReader();
        private NetworkState _networkState;
        List<SignedInGamer> _joinedPlayers = new List<SignedInGamer>();

        public NetworkScreen(Game game, List<SignedInGamer> joinedPlayers, NetworkState networkState)
        {
            _joinedPlayers = joinedPlayers;
            _game = game;
            _networkState = networkState;
        }

        public override void Initialize()
        {
            if (_networkState == NetworkState.Client)
            {
                JoinSession();
            }
            else if (_networkState == NetworkState.Host)
            {
                HostSession();
            }
        }

        private void HostSession()
        {
            try
            {
                Debug.WriteLine("Initializing host");
                _networkSession = NetworkSession.Create(NetworkSessionType.SystemLink, _joinedPlayers, 31, 0, new NetworkSessionProperties());
                HookSessionEvents();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                DisposeSession();
            }
        }

        private void JoinSession()
        {
            try
            {
                Debug.WriteLine("Initializing client");
                // Search for sessions.
                using (AvailableNetworkSessionCollection availableSessions = NetworkSession.Find(NetworkSessionType.SystemLink, _joinedPlayers, new NetworkSessionProperties()))
                {
                    if (availableSessions.Count > 0)
                    {
                        try
                        {
                            _networkSession = NetworkSession.Join(availableSessions[0]);
                            HookSessionEvents();
                        }
                        catch (Exception e)
                        {
                            Debug.WriteLine(e.Message);
                        }
                    }
                    else
                    {
                        DisposeSession();
                    }
  
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                DisposeSession();
            }
        }

        private void DisposeSession()
        {

            if(_networkSession != null)
            {
                _networkSession.Dispose();
            }
            ScreenManager.PreviousScreen();
            Debug.WriteLine("Networksession disposed");
            
        }

        private void HookSessionEvents()
        {
            _networkSession.GamerJoined += gamerJoinedEventHandler;
            _networkSession.SessionEnded += sessionEndedEventHandler;
        }

        private void gamerJoinedEventHandler(object sender, GamerJoinedEventArgs e)
        {
            //new player joins, store player in NetworkGamer.Tag
            if (e.Gamer.IsLocal)
            {
                LocalPlayer localPlayer = new LocalPlayer(_game);
                localPlayer.Name = e.Gamer.Gamertag;
                
                e.Gamer.Tag = localPlayer;

                Debug.WriteLine("Player created: " + e.Gamer.Gamertag);
            }
            else
            {
                Player player = new Player(_game);
                player.Name = e.Gamer.Gamertag;
                
                e.Gamer.Tag = player;
                
                Debug.WriteLine("Remote Player created: " + e.Gamer.Gamertag);
            }
        }

        private void sessionEndedEventHandler(object sender, NetworkSessionEndedEventArgs e)
        {
            Debug.WriteLine(e.EndReason.ToString());
            DisposeSession();
        }

        public override void Update(GameTime gameTime)
        {
            UpdateNetworkSession();
        }

        private void UpdateNetworkSession()
        {
            if (_networkSession != null)
            {
                if (!_networkSession.IsDisposed)
                {
                    //if machine is client machine
                    if (!_networkSession.IsHost)
                    {
                        //ClientSendDataToHost();
                        //ClientReceiveDataFromHost();
                    }
                    else if (_networkSession.IsHost)
                    {
                        //HostReceiveDataFromClient();
                        //HostSendDataToClient();
                    }

                    // Update the underlying session object.
                    _networkSession.Update();
                }
            }
            
        }

        public override void HandleInput(GameTime gameTime)
        {
            if (Input.MenuBack())
            {
                DisposeSession();
            }
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch sb = ScreenManager.SpriteBatch;
        }

        private void ClientSendDataToHost()
        {
            foreach (LocalNetworkGamer g in _networkSession.LocalGamers)
            {
                LocalPlayer localPlayer = g.Tag as LocalPlayer;
                Int32 velocity = (Int32)localPlayer.Ship.Velocity;


                _packetWriter.Write(g.Id);
                _packetWriter.Write(localPlayer.Ship.Position);
                _packetWriter.Write(localPlayer.Ship.Rotation);
                _packetWriter.Write(localPlayer.Ship.IsShooting);
                _packetWriter.Write(velocity);
                g.SendData(_packetWriter, SendDataOptions.InOrder, _networkSession.Host);
            }

        }

        private void ClientReceiveDataFromHost()
        {
            LocalNetworkGamer client = _networkSession.LocalGamers.First<LocalNetworkGamer>();
            //check if data is available
            while (client.IsDataAvailable)
            {
                //store sender
                NetworkGamer sender;
                client.ReceiveData(_packetReader, out sender);


                // This packet contains data about all the players in the session.
                // We keep reading from it until we have processed all the data.
                while (_packetReader.Position < _packetReader.Length)
                {
                    // Read the state of one player from the network packet.
                    byte gamerId = _packetReader.ReadByte();
                    Vector3 position = _packetReader.ReadVector3();
                    Quaternion rotation = _packetReader.ReadQuaternion();
                    Boolean isShooting = _packetReader.ReadBoolean();
                    Int32 velocity = _packetReader.ReadInt32();

                    // Look up which gamer this state refers to.
                    NetworkGamer remoteGamer = _networkSession.FindGamerById(gamerId);

                    // This might come back null if the gamer left the session after
                    // the host sent the packet but before we received it. If that
                    // happens, we just ignore the data for this gamer.
                    if (remoteGamer != null && !remoteGamer.IsLocal)
                    {
                        // Update our local state with data from the network packet.
                        Player remotePlayer = remoteGamer.Tag as Player;
                        remotePlayer.Ship.Position = position;
                        remotePlayer.Ship.Rotation = rotation;
                        remotePlayer.Ship.IsShooting = isShooting;
                        remotePlayer.Ship.Velocity = (float)velocity;
                    }

                }

            }

        }

        private void UpdateClient()
        {
            foreach (NetworkGamer g in _networkSession.RemoteGamers)
            {
                Player p = g.Tag as Player;
                p.Ship.Update();
            }
        }

        private void HostReceiveDataFromClient()
        {
            LocalNetworkGamer server = _networkSession.Host as LocalNetworkGamer;
            //check if data is available
            while (server.IsDataAvailable)
            {
                //store sender
                NetworkGamer sender;
                server.ReceiveData(_packetReader, out sender);
                if (sender.Machine != server.Machine)
                {
                    // This packet contains data about all the players in the session.
                    // We keep reading from it until we have processed all the data.
                    while (_packetReader.Position < _packetReader.Length)
                    {
                        // Read the state of one player from the network packet.
                        byte gamerId = _packetReader.ReadByte();
                        Vector3 position = _packetReader.ReadVector3();
                        Quaternion rotation = _packetReader.ReadQuaternion();
                        Boolean isShooting = _packetReader.ReadBoolean();
                        int velocity = _packetReader.ReadInt32();

                        // Look up which gamer this state refers to.
                        NetworkGamer remoteGamer = _networkSession.FindGamerById(gamerId);

                        // This might come back null if the gamer left the session after
                        // the host sent the packet but before we received it. If that
                        // happens, we just ignore the data for this gamer.
                        if (remoteGamer != null && !remoteGamer.IsLocal)
                        {
                            // Update our local state with data from the network packet.
                            Player remotePlayer = remoteGamer.Tag as Player;
                            remotePlayer.Ship.Position = position;
                            remotePlayer.Ship.Rotation = rotation;
                            remotePlayer.Ship.IsShooting = isShooting;
                            remotePlayer.Ship.Velocity = (float)velocity;

                        }

                    }
                }

            }
        }

        private void HostSendDataToClient()
        {
            if (_networkSession.RemoteGamers.Count > 0)
            {
                _packetWriter.Flush();

                foreach (NetworkGamer g in _networkSession.AllGamers)
                {
                    Player p = g.Tag as Player;
                    Int32 velocity = (Int32)p.Ship.Velocity;

                    _packetWriter.Write(g.Id);
                    _packetWriter.Write(p.Ship.Position);
                    _packetWriter.Write(p.Ship.Rotation);
                    _packetWriter.Write(p.Ship.IsShooting);
                    _packetWriter.Write(velocity);
                }

                LocalNetworkGamer server = _networkSession.Host as LocalNetworkGamer;
                if (_packetWriter.Length > 0)
                {
                    server.SendData(_packetWriter, SendDataOptions.InOrder);
                }
            }
        }
    }
}