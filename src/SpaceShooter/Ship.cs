using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace SpaceShooter
{
    public class Ship : MoveableGameObject, ICollidable
    {
        private InputManager _input;

        private Model _shipModel;
        private Model _missileModel;
        private PlayerIndex _playerIndex;
        private List<Missile> _missiles;
        private List<Missile> _removeList;
        private List<Missile> _activeMissiles;
        private List<Missile> _inactiveMissiles;

        private bool _isColliding;
        private bool _isLocal;

        private List<Bound> _bounds;
        private float _mass;
        private float _additionalVelocity;
        private Quaternion _additionalRotation;

        private int _health;
        private float _energy;

        //control vars
        private bool _isShooting;
        

        public Ship(Game game) : base(game)
        {
            // TODO: Construct any child components here

            // list with all missiles witch are currently flying through the battlefield
            _missiles = new List<Missile>();

            //list with misilles that are currently being charged and are flying with the ship
            _activeMissiles = new List<Missile>();

            _inactiveMissiles = new List<Missile>();
            //list with inactive missiles witch are ready to be used by the ship

            _removeList = new List<Missile>();

            _isColliding = false;
            _isLocal = false;
            _mass = 250f;
            _energy = 100f;
            _additionalVelocity = 0f;
            _additionalRotation = Quaternion.Identity;
            _isShooting = false;
            _health = 100;

            
           
            CreateBounds();

            this.Velocity = 0f;
            this.Scale = 2f;
        }

        public void setMissiles(int numberOfMissiles)
        {
            for (uint i = 0; i < numberOfMissiles; i++)
            {
                Missile missile = new Missile(this.Game, _missileModel);
                _inactiveMissiles.Add(missile);
            }
        }


        public override void Update()
        {
            foreach (Missile missile in _missiles)
            {
                missile.Update();

                //if missiles either dead or out of range... put them in the removelist
                if (missile.Dead == true || Vector3.Distance(this.Position, missile.Position) > 250000f)
                {
                    _removeList.Add(missile);
                }

            }

            foreach (Missile missile in _removeList)
            {
                _missiles.Remove(missile);
                _inactiveMissiles.Add(missile);
            }

            _removeList.Clear();

           

           
            if (_energy < 100f)
            {
                _energy += 0.1f;
                if (_energy > 100f)
                {
                    _energy = 100f;
                }
            }

            
            //update the boundingspheres according to the ships position and rotation   
            foreach (Bound bound in _bounds)
            {
                bound.Update(this.Position, this.Rotation);
            }

            base.Update();

            //should the ship be suffering from a collision reposition according to the velocity and rotation of the colliding object
            Vector3 additionalDirection = Vector3.Transform(Vector3.Forward, _additionalRotation);
            this.Position += additionalDirection * _additionalVelocity;

            if (_additionalVelocity > 0)
            {
                _additionalVelocity -= 0.2f;
            }
            if (_additionalVelocity < 0)
            {
                _additionalVelocity += 0.2f;
            }

            if (_additionalVelocity < 0.2f && _additionalVelocity > -0.2f)
            {
                _additionalVelocity = 0;
            }

            Shoot();
        
        }

        public void Draw(Camera camera)
        {
            Matrix[] transforms = new Matrix[_shipModel.Bones.Count];
            _shipModel.CopyAbsoluteBoneTransformsTo(transforms);

            //turn this on to get a nice view on the ships boundingspheres
            /*
            foreach (Bound bound in _bounds)
            {
                BoundingSphereRenderer.Render(bound.BoundingSphere, Game.GraphicsDevice, camera.View, camera.Projection, Color.Aqua);
            }
            */

            foreach (ModelMesh mesh in _shipModel.Meshes)
            {
     
                foreach (BasicEffect effect in mesh.Effects)
                {
                    
                    effect.EnableDefaultLighting();
                    effect.World = this.World;
                    effect.View = camera.View;
                    effect.Projection = camera.Projection;

                }

                mesh.Draw();
            }

            foreach (Missile missile in _missiles)
            {
                missile.Draw(camera);
            }
            foreach (Missile aMissile in _activeMissiles)
            {
                aMissile.Draw(camera);
            }
            
            
        }
       
        public void Collide(float targetVelocity, Quaternion targetRotation, float targetMass)
        {
            //after colliding reposition the ship a little to its previous position so it will not get stuck in the other object
            this.Position -= this.Direction * this.Velocity;

            //we dont want the collision to keep happening so we check if colliding is false before handeling the collision
            if (_isColliding == false)
            {
                //using the colliding objects speed and rotation we add an extra rotation and velocity to the ship to handle the impact
                
                float thisSpeed = this.Velocity;
                float otherSpeed = targetVelocity;

                if (thisSpeed < 0)
                {
                    thisSpeed *= -1;
                }
                if (otherSpeed < 0)
                {
                    otherSpeed *= -1;
                }


                float N = _mass + thisSpeed;
                float targetN = targetMass + otherSpeed;
                float Ntot = N + targetN;

                _additionalVelocity = (targetN / Ntot) * targetVelocity * 1.5f;

                this.Velocity = (_mass / (_mass * targetMass)) * this.Velocity;


                _additionalRotation = targetRotation;
                _isColliding = true;
            }
        }

        #region boundingspheres
        
        private void CreateBounds()
        {
            // the boundingspheres all have a size and a set offset from the ships center using the bound class we add some extra functionality to the boundinsphere class 

            _bounds = new List<Bound>();
            //hull spheres
            Bound b1 = new Bound(this.Scale * 75f);
            b1.OffSet = new Vector3(0, this.Scale * 50, this.Scale * -525);
            _bounds.Add(b1);

            Bound b2 = new Bound(this.Scale * 125f);
            b2.OffSet = new Vector3(0, this.Scale * 50, this.Scale * -375);
            _bounds.Add(b2);

            Bound b3 = new Bound(this.Scale * 112.5f);
            b3.OffSet = new Vector3(0, this.Scale * 50, this.Scale * -275);
            _bounds.Add(b3);

            Bound b4 = new Bound(this.Scale * 125f);
            b4.OffSet = new Vector3(0, this.Scale * 50, this.Scale * -175);
            _bounds.Add(b4);

            Bound b5 = new Bound(this.Scale * 150f);
            b5.OffSet = new Vector3(0, this.Scale * 50, this.Scale * -50);
            _bounds.Add(b5);

            Bound b6 = new Bound(this.Scale * 187.5f);
            b6.OffSet = new Vector3(0, this.Scale * 50, this.Scale * 125);
            _bounds.Add(b6);

            Bound b7 = new Bound(this.Scale * 237.5f);
            b7.OffSet = new Vector3(0, this.Scale * 150, this.Scale * 500);
            _bounds.Add(b7);

            // right wing spheres
            Bound rw1 = new Bound(this.Scale * 200f);
            rw1.OffSet = new Vector3(this.Scale * 175, this.Scale * 50, this.Scale * 225);
            _bounds.Add(rw1);

            Bound rw2 = new Bound(this.Scale * 175f);
            rw2.OffSet = new Vector3(this.Scale * 400, this.Scale * 50, this.Scale * 250);
            _bounds.Add(rw2);

            Bound rw3 = new Bound(this.Scale * 125f);
            rw3.OffSet = new Vector3(this.Scale * 525, this.Scale * 50, this.Scale * 275);
            _bounds.Add(rw3);

            Bound rw4 = new Bound(this.Scale * 100f);
            rw4.OffSet = new Vector3(this.Scale * 650, this.Scale * 50, this.Scale * 325);
            _bounds.Add(rw4);

            // left wing bounding spheres
            Bound lw1 = new Bound(this.Scale * 200f);
            lw1.OffSet = new Vector3(this.Scale * -175, this.Scale * 50, this.Scale * 225);
            _bounds.Add(lw1);

            Bound lw2 = new Bound(this.Scale * 175f);
            lw2.OffSet = new Vector3(this.Scale * -400, this.Scale * 50, this.Scale * 250);
            _bounds.Add(lw2);

            Bound lw3 = new Bound(this.Scale * 125f);
            lw3.OffSet = new Vector3(this.Scale * -525, this.Scale * 50, this.Scale * 275);
            _bounds.Add(lw3);

            Bound lw4 = new Bound(this.Scale * 100f);
            lw4.OffSet = new Vector3(this.Scale * -650, this.Scale * 50, this.Scale * 325);
            _bounds.Add(lw4);

        }
        #endregion        

        #region shooting
        private void Shoot()
        {
            // the shoot fucntion makes it easy to send all the shooting data we need on ships over network by only using 1 bool (considering we have the ships position and rotation)

            if (_isShooting == true && _activeMissiles.Count == 0)
            {
                activateMissiles();
            }

            else if (_isShooting == true)
            {
                Charge();
            }

            else if (_isShooting == false && _activeMissiles.Count > 0)
            {
                Fire();
            }

        }

        private void activateMissiles()
        {
            if (_energy >= 15f)
            {
                _energy -= 15f;
                if (_inactiveMissiles.Count >= 2)
                {
                    //activates 2 missiles from the innactiveMissile list, this allowes us to keep reusing our missiles
                    Missile leftMissile = ReuseMissile();
                    leftMissile.missileOffset = new Vector3(this.Scale * -400, this.Scale * 50, this.Scale * -175);
                    leftMissile.Position = this.Position + Vector3.Transform(leftMissile.missileOffset, this.Rotation);
                    _activeMissiles.Add(leftMissile);

                    Missile rightMissile = ReuseMissile();
                    rightMissile.missileOffset = new Vector3(this.Scale * 400, this.Scale * 50, this.Scale * -175);
                    rightMissile.Position = this.Position + Vector3.Transform(rightMissile.missileOffset, this.Rotation);
                    _activeMissiles.Add(rightMissile);

                    Console.WriteLine("reusing missile");
                }
                // highly unlikely to actually get to the else statement considering the ship has limited energy and a big innactive list but just in case...
                else
                {
                    Missile leftMissile = new Missile(this.Game, _missileModel);
                    leftMissile.missileOffset = new Vector3(this.Scale * -400, this.Scale * 50, this.Scale * -175);
                    leftMissile.Position = this.Position + Vector3.Transform(leftMissile.missileOffset, this.Rotation);
                    _activeMissiles.Add(leftMissile);

                    Missile rightMissile = new Missile(this.Game, _missileModel);
                    rightMissile.missileOffset = new Vector3(this.Scale * 400, this.Scale * 50, this.Scale * -175);
                    rightMissile.Position = this.Position + Vector3.Transform(rightMissile.missileOffset, this.Rotation);
                    _activeMissiles.Add(rightMissile);
                }
            }
        }

        private Missile ReuseMissile()
        {
            Missile missile = _inactiveMissiles[0];
            _inactiveMissiles.Remove(missile);
            missile.Reset();
            return missile;
        }

        private void Charge()
        {
            // Charges the missiles up and repositions them according to the ships position, rotation and the missiles offset wich is either left or right from the ship's front
            foreach (Missile missile in _activeMissiles)
            {
                if (missile.Radius < 750)
                {
                    missile.Radius += 5f;            
                }
                missile.Position = this.Position + Vector3.Transform(missile.missileOffset, this.Rotation);
                missile.Update();
            }
        }

        private void Fire()
        {
            // FIRE EVERYTHINGG!!! *spit flying all around*  

            //pushes the active missiles in the missile list freeing them from the ships grasp 
            foreach (Missile missile in _activeMissiles)
            {
                //gives the missiles a set speed and the ships rotation when fired
                missile.Velocity = 600;
                missile.Rotation = this.Rotation;
                _missiles.Add(missile);
            }
            _activeMissiles.Clear();
        }

        #endregion

        #region input

        // handles all input via the input class
        public void HandleInput()
        {
            float yaw = 0f;
            float pitch = 0f;
            float roll = 0f;

            GamePadState state = Input.GetGamePadState(_playerIndex);

            //pitch
            pitch -= 0.025f * Input.ShipPitch(_playerIndex);

            //roll
            roll -= 0.025f * Input.ShipRoll(_playerIndex);

            //yaw
            yaw += 0.025f * Input.ShipJaw(_playerIndex);

            //velocity
            if (Input.ShipDecelerate(_playerIndex) > 0)
            {
                if (this.Velocity > -100f)
                {
                    this.Velocity -= Input.ShipDecelerate(_playerIndex) * 2f;
                }
            }
            else if (Input.ShipAccelerate(_playerIndex) > 0)
            {
                if (this.Velocity < 200f)
                {
                    this.Velocity += Input.ShipAccelerate(_playerIndex) * 2f;
                }       
            }
            else
            {
                if (this.Velocity > 0f)
                {
                    this.Velocity -= 0.50f;
                    if (this.Velocity < 0f)
                    {
                        this.Velocity = 0f;
                    }
                }

                else if (this.Velocity < 0f)
                {
                    this.Velocity += 0.50f;
                    if (this.Velocity > 0f)
                    {
                        this.Velocity = 0f;
                    }
                }
            }

            if (state.Buttons.A == ButtonState.Pressed)
            {
                _isShooting = true;        
            }
            else
            {
                _isShooting = false;
            }
            this.Yaw = yaw;
            this.Pitch = pitch;
            this.Roll = roll;

        }
        #endregion

        public void Respawn(Vector3 respawnLocation)
        {
            // resets the ship's state and sets it position at a spawn location

            this.Yaw = 0f;
            this.Pitch = 0f;
            this.Roll = 0f;

            this.Position = respawnLocation;
            this.Rotation = Quaternion.Identity;
            this.Velocity = 0f;
            _activeMissiles.Clear();
            _additionalRotation = Quaternion.Identity;
            _additionalVelocity = 0f;
            _health = 100;
            _energy = 100f;
        }

        //getters and setters
        public Model ShipModel
        {
            get { return _shipModel; }
            set { _shipModel = value; }
        }

        public Model MissileModel
        {
            get { return _missileModel; }
            set { _missileModel = value; }
        }

        public PlayerIndex PlayerIndex
        {
            get { return _playerIndex; }
            set { _playerIndex = value; }
        }

        public List<Missile> Missiles
        {
            get { return _missiles; }
            set { _missiles = value; }
        }

        public float Mass
        {
            get { return _mass; }
            set { _mass = value; }
        }

        public Quaternion AdditionalRotation
        {
            get { return _additionalRotation; }
            set { _additionalRotation = value; }
        }

        public float AdditionalVelocity
        { 
            get { return _additionalVelocity; }
            set { value = _additionalVelocity; }
        }

        public bool IsColliding
        {
            get { return _isColliding; }
            set { _isColliding = value; }
        }
        public bool IsShooting
        {
            get { return _isShooting; }
            set { _isShooting = value; }
        }
        public bool IsLocal
        {
            get { return _isLocal; }
            set { _isLocal = value; }
        }

        public List<Bound> Bounds
        {
            get { return _bounds; }
            set { _bounds = value; }
        }

        public int Health
        {
            get { return _health; }
            set { _health = value; }
        }

        public float Energy
        {
            get { return _energy; }
            set { _energy = value; }
        }

        public InputManager Input
        {
            get { return _input; }
            set { _input = value; }
        }
    }
}
