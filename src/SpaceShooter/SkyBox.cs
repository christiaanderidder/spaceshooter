﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpaceShooter
{
    public class SkyBox : DrawableGameComponent
    {
        Model _skybox;
        Game _game;
        float _scale;

        public SkyBox(Game game)
            : base(game)
        {
            _game = game;
            _scale = 25000;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public void Draw(Camera cam)
        {
            Matrix[] transforms = new Matrix[_skybox.Bones.Count];
            _skybox.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in _skybox.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    //effect.EnableDefaultLighting();
                    effect.World = Matrix.CreateScale(_scale) * Matrix.CreateTranslation(cam.Position);
                    effect.View = cam.View;
                    effect.Projection = cam.Projection;
                    
                }

                mesh.Draw();
            }

        }

        public Model Model
        {
            get { return _skybox; }
            set { _skybox = value; }
        }
        public float Scale
        {
            get { return _scale; }
            set { _scale = value; }
        }


    }
}
