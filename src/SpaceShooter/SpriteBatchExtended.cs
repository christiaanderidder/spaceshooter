﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace SpaceShooter
{
    public class SpriteBatchExtended : SpriteBatch
    {
        public const string XButton = "[X]";
        public const string YButton = "[Y]";
        public const string AButton = "[A]";
        public const string BButton = "[B]";
        public const string RightTrigger = "[RTRIGGER]";
        public const string LeftTrigger = "[LTRIGGER]";
        public const string Back = "[BACK]";
        public const string DPad = "[DPAD]";
        public const string Guide = "[GUIDE]";
        public const string LeftShoulder = "[LSHOULDER]";
        public const string LeftThumb = "[LTHUMB]";
        public const string RightShoulder = "[RSHOULDER]";
        public const string RightThumb = "[RTHUMB]";
        public const string Start = "[START]";

        public SpriteFont fntButton { get; set; }

        public SpriteBatchExtended(GraphicsDevice gd) : base(gd)
        {

        }

        public virtual void DrawGlyphString(SpriteFont spriteFont, string text, Vector2 position, Color color)
        {
            Vector2 outPos = position;
            StringBuilder strBuffer = new StringBuilder(0, text.Length);
            bool buttonTxt = false;
            float butScale;
            float offY;
            char letter;

            butScale = spriteFont.MeasureString("|").Y / (fntButton.MeasureString("$").Y / 2);
            offY = ((fntButton.MeasureString("$").Y * butScale) / 4);

            for (int ch = 0; ch < text.Length; ch++)
            {
                letter = text[ch];

                if (buttonTxt)// We are building a button string.
                {
                    strBuffer.Append(letter);

                    if (letter == ']')
                    {
                        string str = ChangeButtonType(strBuffer.ToString());
                        if (!string.IsNullOrEmpty(str))
                        {
                            outPos.Y -= offY;
                            base.DrawString(fntButton, str, outPos, Color.White, 0f, Vector2.Zero, butScale, SpriteEffects.None, 0f);
                            outPos.X += (fntButton.MeasureString(str).X * butScale);
                            outPos.Y += offY;
                            strBuffer = new StringBuilder(0, text.Length);
                        }
                        buttonTxt = false;
                    }
                }
                else
                {// We are building a text string.
                    switch (letter)
                    {
                        case '[':
                            if (!String.IsNullOrEmpty(strBuffer.ToString()))
                            {
                                base.DrawString(spriteFont, strBuffer, outPos, color, 0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0f);
                                outPos.X += spriteFont.MeasureString(strBuffer).X;
                                strBuffer = new StringBuilder(0, text.Length); ;
                            }
                            buttonTxt = true;
                            strBuffer.Append(letter);
                            break;
                        case '\n':
                            if (!String.IsNullOrEmpty(strBuffer.ToString()))
                            {
                                base.DrawString(spriteFont, strBuffer, outPos, color, 0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0f);
                                strBuffer = new StringBuilder(0, text.Length);;
                            }
                            outPos.X = position.X;
                            outPos.Y += spriteFont.LineSpacing;
                            break;
                        default:
                            strBuffer.Append(letter);
                            break;
                    }
                }
            }

            if (!String.IsNullOrEmpty(strBuffer.ToString()))
                base.DrawString(spriteFont, strBuffer, outPos, color);
        }

        public string ChangeButtonType(string theButton)
        {
            switch (theButton.ToUpper())
            {
                case XButton: { return "&"; }
                case YButton: { return "("; }
                case AButton: { return "'"; }
                case BButton: { return ")"; }
                case RightTrigger: { return "+"; }
                case LeftTrigger: { return ","; }
                case Back: { return "#"; }
                case DPad: { return "!"; }
                case Guide: { return "$"; }
                case LeftShoulder: { return "-"; }
                case LeftThumb: { return " "; }
                case RightShoulder: { return "*"; }
                case RightThumb: { return "\""; }
                case Start: { return "%"; }
            }
            return "";
        }
    }
}
